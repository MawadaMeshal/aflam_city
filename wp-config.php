<?php
/**
 * إعدادات الووردبريس الأساسية
 *
 * عملية إنشاء الملف wp-config.php تستخدم هذا الملف أثناء التنصيب. لا يجب عليك
 * استخدام الموقع، يمكنك نسخ هذا الملف إلى "wp-config.php" وبعدها ملئ القيم المطلوبة.
 *
 * هذا الملف يحتوي على هذه الإعدادات:
 *
 * * إعدادات قاعدة البيانات
 * * مفاتيح الأمان
 * * بادئة جداول قاعدة البيانات
 * * المسار المطلق لمجلد الووردبريس
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** إعدادات قاعدة البيانات - يمكنك الحصول على هذه المعلومات من مستضيفك ** //

/** اسم قاعدة البيانات لووردبريس */
define('DB_NAME', 'aflam_city');

/** اسم مستخدم قاعدة البيانات */
define('DB_USER', 'dev');

/** كلمة مرور قاعدة البيانات */
define('DB_PASSWORD', 'lZnco8GXlUl7K3IU');

/** عنوان خادم قاعدة البيانات */
define('DB_HOST', 'localhost');

/** ترميز قاعدة البيانات */
define('DB_CHARSET', 'utf8mb4');

/** نوع تجميع قاعدة البيانات. لا تغير هذا إن كنت غير متأكد */
define('DB_COLLATE', '');

/**#@+
 * مفاتيح الأمان.
 *
 * استخدم الرابط التالي لتوليد المفاتيح {@link https://api.wordpress.org/secret-key/1.1/salt/}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',Jg6eadgnvzM]-ZUY_9IFUt]5Ku9e2}rdyhHqaY8wj>6({0[HeDz2fuT}oumuPcI');
define('SECURE_AUTH_KEY',  '0I$*)>joS?X4V_e69B.3O#hKNRp4hI*G{U9w= Ly>a <|h`Dnk:LC^N064Y0#lbw');
define('LOGGED_IN_KEY',    'c `85m.C}l E~Igo+-;,:LcIDi7f&cuT;lf$|x_z<G-pu>n1K;T#e|,<y83^$zpR');
define('NONCE_KEY',        'FuhEIFt:cLVLGhFF9Y*lYm+W=AgkNiO,zeCGsaHwoNEJ1 .OK*QB)V+V*qqhAEB`');
define('AUTH_SALT',        'u U|ReKIQjE=7c=U`#Hx^hG&}cD^:?V59rNtf:2aT2<uJW^$vp=:iwWTQ$#UgTvg');
define('SECURE_AUTH_SALT', 'w@Umkd-~Ny5=uTxmrA4Zk=1w,l-QOl4#<Ie*y9[hg_:pGe5+FBiN&C{_r)q<>c?J');
define('LOGGED_IN_SALT',   '(M+>g|e>_g@X<Zge>}~L){|*=~!GjR)S-NK46eyThDZ!r``FhKTWog.lFPf8-T7w');
define('NONCE_SALT',       ':z ?J_^D)`[ef<)~i{Uv<>hWcj%y iwni10TKBNBx%;>uAg(JS|sF[(v2Uo6!&Bj');

/**#@-*/

/**
 * بادئة الجداول في قاعدة البيانات.
 *
 * تستطيع تركيب أكثر من موقع على نفس قاعدة البيانات إذا أعطيت لكل موقع بادئة جداول مختلفة
 * يرجى استخدام حروف، أرقام وخطوط سفلية فقط!
 */
$table_prefix  = 'wp_';

/**
 * للمطورين: نظام تشخيص الأخطاء
 *
 * قم بتغييرالقيمة، إن أردت تمكين عرض الملاحظات والأخطاء أثناء التطوير.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* هذا هو المطلوب، توقف عن التعديل! نتمنى لك التوفيق. */

/** المسار المطلق لمجلد ووردبريس. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** إعداد متغيرات الووردبريس وتضمين الملفات. */
require_once(ABSPATH . 'wp-settings.php');