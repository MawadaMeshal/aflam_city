<?php get_header(); 
    
$taxonomy = 'movie_categories';
$terms = get_terms($taxonomy); // Get all terms of a taxonomy
$tag ='';
if ( $terms && !is_wp_error( $terms ) ) :
    foreach ( $terms as $term ) {
        $tag.='<a href="'.esc_url(home_url("/")).'movie">'.$term->name.'</a>,';

} 
$tag = trim($tag,',');
?>
    
<?php endif;?>
<div class="main-wrap">
<?php if ( have_posts() ) :

	while ( have_posts() ) : the_post();


    $movie_arr =array("movie_trailer",
                    "movie_rating",
                    "movie_cust",
                    "movie_duration",
                    "movie_country",
                    "movie_language",
                    "movie_gallery",
					"movie_poster",
					"movie_genres"
                );
    foreach ($movie_arr as $key => $value) {
       $data = get_post_meta( get_the_ID(), $value, 1 );
       $movie_arr[$value]  = !empty( $data) ?  $data : ''; 
    }

    extract($movie_arr);
	$date = get_the_date('d F , Y');


?>	

	<div class="section section-padding video-single-section">
        <div class="container">
            <div class="video-single">
                <div class="row">
                    <div class="col-xs-12">

                    <?php
						if ( has_post_thumbnail()) { ?>
                        <div class="thumb-wrap single-thumb">
                            <?php the_post_thumbnail('full', array('class' => 'img-responsive'));?>
                            <div class="thumb-hover">
                                <a class="play-video play-video-single" href="<?php print esc_url($movie_trailer); ?>"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    <?php } ?>
                        <div class="content-wrap">
                            <div class="video-thumb">
                                <img src="<?php print esc_url($movie_poster); ?>" alt="<?php esc_html_e('Movie Thumb','bigshow'); ?>">
                            </div>
                            <div class="video-details xs-top-40">
                                <div class="single-section">
                                    <h3 class="video-title"><?php the_title(); ?></h3>
                                    <p class="video-release-date"><?php print esc_html($date); ?></p>
                                    <div class="ratings-wrap">
                                        <span class="rating"><?php print esc_html($movie_rating); ?></span>
                                        <div class="expanded-rating">
                                            <div class="star-rating">
                                            <?php $on =''; for($i=0;$i<10;$i++){
                                            	$on = ($movie_rating > $i) ? "on": "";
                                            	print '<span class="star '.$on.'"></span>';
                                            } ?>
                                            </div>
                                            <div class="user-voted">
                                                <i class="fa fa-user"></i> <?php print esc_html__("التقييم","bigshow");?> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="video-attributes">
										<?php 
											print '<p class="cast"><label>'.esc_html__("الممثلين : ","bigshow").'</label>'.esc_html($movie_cust).'</p>';
											print '<p class="duration"><label>'.esc_html__("الوقت:","bigshow").'</label> '." ".esc_html($movie_duration).'</p>';
											print '<p class="genre"><label>'.esc_html__("التصنيف:","bigshow").'</label> '.esc_html($movie_genres).'</p>';
											print '<p class="country"><label>'.esc_html__("الدولة:","bigshow").'</label> '." ".esc_html($movie_country).'</p>';
											print '<p class="language"><label>'.esc_html__("اللغة:","bigshow").'</label> '." ".esc_html($movie_language).'</p>';
										   
										?>
                                    </div>
                                    <?php if ( shortcode_exists( 'cp_post_share' ) ) { ?>
											<div class="share-on">
												 <label><?php esc_html_e("مشاركة: ","bigshow"); ?></label>
												<?php print do_shortcode('[cp_post_share]'); ?>
											</div>
									<?php } ?>
                                </div>
                                <div class="single-section video-entry">
                                    <h3 class="single-section-title"><?php esc_html_e("قصة الفيلم","bigshow"); ?></h3>
                                    <div class="section-content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                <?php if(is_array($movie_gallery)){ ?>
                                    <div class="single-section video-entry">
                                        <h3 class="single-section-title"><?php esc_html_e("صور و فيديو","bigshow"); ?></h3>
                                        <div class="section-content">
                                            <div id="single-gallery-1" class="owl-carousel single-gallery-slider">
                                            <?php 
											
												foreach ($movie_gallery as $key => $value) {
													print '<img class="img-responsive" src="'.esc_url($value).'" alt="'.esc_html__("Image","bigshow").'">';
												}
                                            ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile;
    endif;
    
?>
    <div class="section section-padding top-padding-normal movie-section">
        <?php bigshow_latest_movie(); ?>
    </div>
</div>
<?php get_footer(); ?>