<?php 
get_header();
/**
 * The template for displaying archive pages
 * @package WordPress
 * @subpackage Bigshow
 */
?>
<div class="main-wrap">
    <div class="section section-padding news-list-section">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-12">
                    <div class="news-list">
                    	<?php
    						if ( have_posts() ) :
    							while ( have_posts() ) : the_post();
    								get_template_part( 'template-parts/content', get_post_format());
    							endwhile;
    						else:
    							get_template_part( 'template-parts/content', 'none' );
    						endif;
    					?>
                        <nav class="navigation pagination" role="navigation">
                             <div class="nav-links">
                                <?php 
									the_posts_pagination(
										array(
											'prev_text' => '<i class="fa fa-caret-left"></i>',
											'next_text' => '<i class="fa fa-caret-right"></i>', 
											'mid_size' => 2,
											'screen_reader_text'=>' ' 
										) 
									); 
								?>
                            </div>
                        </nav>
                    </div>
            	</div>
            	<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12">
                    
                    <?php get_sidebar(); ?>	
            		
            	</div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
