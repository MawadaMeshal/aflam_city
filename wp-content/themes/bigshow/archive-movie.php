<?php 
get_header();
/**
 * The template for displaying movie Category
 * @package WordPress
 * @subpackage Bigshow
 */
?>
<div class="main-wrap">
    <div class="section section-padding video-list-section">
        <div class="container">
            <div class="row">
                <div class="show-listing">
                <?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post(); 
					$movie_arr =array("movie_trailer",
			                    "movie_rating",
			                    
								"movie_poster"
                    );
                     foreach ($movie_arr as $key => $value) {
				       $data = get_post_meta( get_the_ID(), $value, 1 );
				       $movie_arr[$value]  = !empty( $data) ?  $data : ''; 
				    }

				    extract($movie_arr);
				    $date = get_the_date('d F , Y');
				?>
	                    <div class="col-md-3 col-sm-4 col-xs-6">
	                        <div class="video-item">
	                        <?php
								if ( $movie_poster !='') {
							?>
	                            <div class="thumb-wrap">
	                                <img src="<?php print esc_url($movie_poster); ?>" alt="<?php esc_html_e('Movie Thumb','bigshow'); ?>">
	                                <span class="rating"><?php print esc_html($movie_rating); ?></span>
	                                <div class="thumb-hover">
	                                    <a class="play-video" href="<?php print esc_url($movie_trailer); ?>"><i class="fa fa-play"></i></a>
	                                </div>
	                            </div>
	                        <?php } ?>
	                            <div class="video-details">
	                                <h4 class="video-title"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
	                                <p class="video-release-on"><?php print esc_html($date); ?></p>
	                            </div>
	                        </div>
	                    </div>
	            <?php 
	                endwhile; 

	                endif; 
	            ?>
            
                    
                    <div class="col-xs-12">
                        <!-- Video Pagination -->
                        <nav class="navigation pagination" role="navigation">
                            <div class="nav-links">
                                <?php print bigshow_pagination('<i class="fa fa-caret-left"></i>','<i class="fa fa-caret-right"></i>');?>
                            </div>
                        </nav>
                        <!-- Video Pagination End -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
