<?php get_header(); ?>

    <div class="main-wrap">
<?php if ( have_posts() ) :

    while ( have_posts() ) : the_post();

        $first_team = get_post_meta($post->ID, 'first_team', true);
        $second_team = get_post_meta( $post->ID,'second_team', true );
        $commentator = get_post_meta( $post->ID,'commentator', true );
        $football_stadium = get_post_meta( $post->ID,'football_stadium', true );
        $time = get_post_meta( $post->ID,'time', true );
        $url = get_post_meta( $post->ID,'url', true );


        ?>

        <div class="section section-padding video-single-section">
            <div class="container">
                <div class="video-single">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php
                            if ( has_post_thumbnail()) { ?>
                                <div class="thumb-wrap single-thumb">
                                    <?php the_post_thumbnail('full', array('class' => 'img-responsive'));?>
                                    <div class="thumb-hover">
                                        <a class="play-video play-video-single" href="<?php print esc_url($url); ?>"><i class="fa fa-play"></i></a>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="content-wrap">
                                <div class="video-details xs-top-40">
                                    <div class="single-section">
                                        <h3 class="video-title"><?php the_title(); ?></h3>
                                        <p class="video-release-date"><?php print esc_html($date); ?></p>
                                        <div class="video-attributes">
                                            <?php
                                            print '<p class="cast"><label>'.esc_html__("الفريق الأول : ","bigshow").'</label>'.$first_team.'</p>';
                                            print '<p class="duration"><label>'.esc_html__("الفريق الثاني:","bigshow").'</label> '." ".esc_html($second_team).'</p>';
                                            print '<p class="genre"><label>'.esc_html__("المعلق:","bigshow").'</label> '.esc_html($commentator).'</p>';
                                            print '<p class="country"><label>'.esc_html__("الملعب:","bigshow").'</label> '." ".esc_html($football_stadium).'</p>';
                                            print '<p class="language"><label>'.esc_html__("التوقيت:","bigshow").'</label> '." ".esc_html($time).'</p>';

                                            ?>
                                        </div>
                                        <?php if ( shortcode_exists( 'cp_post_share' ) ) { ?>
                                            <div class="share-on">
                                                <label><?php esc_html_e("مشاركة: ","bigshow"); ?></label>
                                                <?php print do_shortcode('[cp_post_share]'); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="single-section video-entry">
                                        <h3 class="single-section-title"><?php esc_html_e("عن المباراة","bigshow"); ?></h3>
                                        <div class="section-content">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile;
endif;

?>

<?php get_footer(); ?>