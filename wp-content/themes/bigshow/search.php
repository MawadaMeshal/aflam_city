<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage Bigshow
 */
 get_header();
 ?>

<div class="main-wrap">
    <div class="section section-padding news-list-section">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-7">
                    <div class="news-list">
                    	<?php
    						if ( have_posts() ) :
    							while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', get_post_format());
								endwhile;
    						?>
    						<nav class="navigation pagination" role="navigation">
	                             <div class="nav-links">
	                                <?php 
										the_posts_pagination(
											array(
												'prev_text' => '<i class="fa fa-caret-left"></i>',
												'next_text' => '<i class="fa fa-caret-right"></i>', 
												'mid_size' => 2,
												'screen_reader_text'=>' ' 
											) 
										); 
									?>
	                            </div>
	                        </nav>
    						<?php	else: ?>
    							<div class="search-form">
									<h2 class="title-404"><?php esc_html_e('No Data Found For : ','bigshow'); ?> <?php print get_search_query(); ?></h2>
									<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.','bigshow'); ?></p>												
									<form action="<?php print esc_url( home_url( '/' ) ); ?> " method="get" class="search-form">
										<div class="search-form-inner">
											<input type="text" name="s" class="form-control form-inner-text" placeholder="<?php print esc_attr__('Search Here', 'bigshow'); ?>" value='<?php print get_search_query(); ?>'>
											<button type="submit" name="search" class="inner-button-style"><i class="fa fa-search"></i></button>
										</div>
									</form>
								</div>
    						<?php endif; ?>
                       
                    </div>
            	</div>
            	<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-5">
                    
                    <?php get_sidebar(); ?>	
            		
            	</div>
            </div>
        </div>
    </div>
</div>
	
<?php get_footer(); ?>
