<?php
/**
* Template Name: VC Page
* @package WordPress
* @subpackage Bigshow
*/
get_header();
?>


<div class="main-wrap">
  
    <div class="container">
      <?php 
        if(have_posts()){
         while ( have_posts() ): the_post();
         the_content();
         endwhile; 
       } else {
         get_template_part( 'template-parts/content', 'none' );
       }
     ?>               
   </div>    
   
</div>    


<?php get_footer();
