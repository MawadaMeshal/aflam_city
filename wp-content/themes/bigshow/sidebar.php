<?php
/**
 * The sidebar containing the main widget area
 * @package WordPress
 * @subpackage Bigshow
 */
	if ( is_active_sidebar( 'right-sidebar' ) ) {
?>
		
	<div class="sidebar">
		<?php dynamic_sidebar('right-sidebar'); ?> 
	</div>
			
<?php }