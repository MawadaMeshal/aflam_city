<?php /* Template Name: Matches */ ?>

<?php get_header(); ?>

        <div class="main-wrap">
            <div class="section section-padding video-list-section">
                <div class="container">
                     <div class="show-listing">
					 
					  <?php
                    $the_query = new WP_Query( array('post_type'=>"matches",
                        'showposts'=>18,
                    ));

                    while($the_query->have_posts()) : $the_query->the_post();
                        $meta = get_post_meta ( get_the_ID(), 'time', true);
						 $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); 
                        ?>
				         <div class="col-md-3 col-sm-4 col-xs-6">
                              <div class="video-item">
                                    <div class="thumb-wrap">
                                        <img src="<?php echo $url?>" alt="Movie Thumb" style="width:500px;height:200px;">
                                        <div class="thumb-hover">
                                            <a class="play-video" href="<?php the_permalink(); ?>"><i class="fa fa-play"></i></a>
                                        </div>
                                    </div>
                                    <div class="video-details">
                                        <h4 class="video-title"><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
                                        <p class="video-release-on"><?php echo $time; ?></p>
                                    </div>
                                </div>
                            </div>

                    <?php endwhile; ?>
                   
                            <div class="col-xs-12">
                                <!-- Video Pagination -->
                                <nav class="navigation pagination" role="navigation">
                                    <div class="nav-links"></div>
                                </nav>
                                <!-- Video Pagination End -->
                            </div>
							
			        </div>
                </div>
            </div>
        </div>
<?php get_footer(); ?>