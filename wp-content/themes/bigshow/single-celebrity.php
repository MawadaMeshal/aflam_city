<?php get_header(); ?>

<div class="main-wrap">
<?php if ( have_posts() ) :


	while ( have_posts() ) : the_post();

    $crew_arr =array("crew_type",
                    "crew_name",
                    "crew_dob",
                    "crew_residence",
                    "crew_country",
                    "crew_gender",
                    "crew_language",
                    "crew_height",
                    "crews_gallery", 
                    "crews_movie"                   
                );
    foreach ($crew_arr as $key => $value) {
       $data = get_post_meta( get_the_ID(), $value, 1 );
       $crew_arr[$value]  = !empty( $data) ?  $data : ''; 
    }

    extract($crew_arr);
	
?>	
    <div class="section section-padding celebrity-single-section">
        <div class="container">
            <div class="celebrity-single">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="content-wrap">
                        
                            <div class="celebrity-thumb">
                            <?php
                                if ( has_post_thumbnail()) { 
                                    the_post_thumbnail('full', array('class' => 'img-responsive')); 
                                }
							?>
                            </div>
                            
                            <div class="celebrity-details">
                                <div class="single-section">
                                    <h3 class="celebrity-name"><?php the_title(); ?></h3>
                                    <p class="celebrity-profession"><?php print esc_html($crew_type); ?></p>
                                    <div class="celebrity-infos">
                                    <?php
                                        print '<p class="birthname"><label>'.esc_html("Birth Name:","bigshow").'</label>'.esc_html($crew_name).'</p>';
                                        print '<p class="birthdate"><label>'.esc_html("Date of Birth:","bigshow").'</label>'.esc_html($crew_dob).'</p>';
                                        print '<p class="residence"><label>'.esc_html("Residence:","bigshow").'</label>'.esc_html($crew_residence).'</p>';
                                        print '<p class="country"><label>'.esc_html("Country:","bigshow").' </label>'.esc_html($crew_country).'</p>';
                                        print '<p class="gender"><label>'.esc_html("Gender:","bigshow").' </label>'.esc_html($crew_gender).'</p>';
                                        print '<p class="language"><label>'.esc_html("Language:","bigshow").'</label>'.esc_html($crew_language).'</p>';
                                        print  '<p class="height"><label>'.esc_html("Height:","bigshow").'</label>'.esc_html($crew_height).'</p>';
                                    ?>
                                    </div>
									<?php if ( shortcode_exists( 'cp_post_share' ) ) { ?>
											<div class="share-on">
												 <label><?php esc_html_e("Share: ","bigshow"); ?></label>
												<?php print do_shortcode('[cp_post_share]'); ?>
											</div>
									<?php } ?>
                                </div>
                                <div class="single-section bio-entry">
                                    <h3 class="single-section-title"><?php esc_html_e("Biography","bigshow");?></h3>
                                    <div class="section-content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                                <div class="single-section">
                                    <h3 class="single-section-title"><?php esc_html_e("Video & Photo","bigshow"); ?></h3>
                                    <div class="section-content">
                                        <div id="single-gallery-1" class="owl-carousel single-gallery-slider">
                                           <?php 
                                            foreach ($crews_gallery as $key => $value) {
                                                print '<img class="img-responsive" src="'.esc_url($value).'" alt="'.esc_html("Image","bigshow").'">';
                                            }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-section">
                                    <h3 class="single-section-title"><?php esc_html_e("Filmography","bigshow"); ?></h3>
                                    <div class="section-content">
                                        <table class="filmography-table">
                                            <tr class="head-tr">
                                                <th colspan="2"><?php esc_html_e("Movie Name","bigshow"); ?></th>
                                                <th><?php esc_html_e("Release Date","bigshow"); ?></th>
                                            </tr>
                                            <?php 
                                                $query = new WP_Query(array('post__in' => $crews_movie,'post_type' => 'movie'));
                                                
                                                if($query->have_posts()):
                                               
                                                while($query-> have_posts()) : $query->the_post();
                                                    $movie_genres = get_post_meta( get_the_ID(), 'movie_genres', 1 );
                                                    $date = get_the_date('d F , Y');
                                                    print '<tr>';
                                                        print '<td class="film-poster">';
                                                            if ( has_post_thumbnail()) {
                                                                print '<a class="film-thumb" href="'.get_the_permalink().'">
                                                                        '.get_the_post_thumbnail(get_the_ID(),'full', array('class' => 'img-responsive')).'</a>';
                                                            }
                                                        print '</td>';
                                                        print '<td class="film-details">';
                                                            print '<div class="film-info">';
                                                                print '<h4 class="film-title"><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
                                                                print '<p class="film-category">'.esc_html($movie_genres).'</p>';
                                                            print '</div>';
                                                        print '</td>';
                                                        print '<td class="film-release">'.esc_html($date).'</td>';
                                                    print '</tr>';
                                                endwhile;
                                                
                                            endif;
                                            
                                            ?>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>