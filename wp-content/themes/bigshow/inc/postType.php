<?php
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {

 // slider post type
     $labels = array(

    "name" => "matches",

    "singular_name" => "matches",

    "menu_name" => "matches",

    "add_new" => "add new",

    "add_new_item" => "add new",

    "edit" => "edit ",

    "edit_item" => "edit",

    "new_item" => " new",

    "view" => "view",

    "view_item" => " view",

    "search_items" => " search ",

    "not_found" => "not found",

    "not_found_in_trash" => "not found in trash",

    );



  $args = array(

    "labels" => $labels,

    "description" => "",

    "public" => true,

    "show_ui" => true,

    "has_archive" => false,
    'menu_icon'   => 'dashicons-image-filter',//admin-users
    "show_in_menu" => true,

    "exclude_from_search" => true,

    "capability_type" => "post",

    "map_meta_cap" => true,

    "hierarchical" => false,

    "rewrite" => array( "slug" => "matches", "with_front" => true ),

    "query_var" => true,

        

    "supports" => array( "title","thumbnail","editor","page-attributes"),

  );

    register_post_type( "matches", $args );
 

    }

?>