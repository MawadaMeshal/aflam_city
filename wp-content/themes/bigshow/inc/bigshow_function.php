<?php
/**
 * The template for custom function
 *
 * @package WordPress
 * @subpackage Bigshow
 * @since Bigshow 1.0
 */
?>
<?php 

	/*
		name: bigshow_pagination
		return : pagination style
	*/
	if(!function_exists('bigshow_pagination')){
		
		function bigshow_pagination($prev = 'Prev', $next = 'Next', $pages='' ,$args=array('class'=>'')) {
			global $wp_query, $wp_rewrite;
			$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
			if($pages==''){
				global $wp_query;
				 $pages = $wp_query->max_num_pages;
				 if(!$pages)
				 {
					 $pages = 1;
				 }
			}
			$pagination = array(
				'base' => @add_query_arg('paged','%#%'),
				'format' => '',
				'total' => $pages,
				'current' => $current,
				'prev_text' => "&laquo;",
				'next_text' => "&raquo;",
				'type' => 'array'
			);
			if( $wp_rewrite->using_permalinks() )
				$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

			if( !empty($wp_query->query_vars['s']) )
				$pagination['add_args'] = array( 's' => get_query_var( 's' ) );
			if(paginate_links( $pagination )!=''){
				$paginations = paginate_links( $pagination );
				$data = '';
				$data.='<ul class="pagination " '.$args["class"].'">';
					$data.='<span class="current-page">Page'.$pagination["current"].' of '.$pagination["total"].'</span>';
					foreach ($paginations as $key => $pg) {
						
						$data.='<li class="page-number">'.$pg.'</li>';
					}
				$data.='</ul>';

				return $data;
				?>

				<?php
			}
		}
	}

	/*===============================================
	*			Function name: bigshow_comment
	*			return : comments
	*
	================================================*/
	
	if(!function_exists("bigshow_comment")):
		function bigshow_comment($comment,$args,$depth){
			$GLOBALS['comment'] = $comment;
			extract($args, EXTR_SKIP);
		?>
			<li class="comment">                   
				<div class="comment-wrap" id="comment-<?php print $comment->comment_ID; ?>">
					<div class="commenter-thumb">
                        <?php  print get_avatar( $comment,80,null,null,array('class'=>array('avatar'))); ?>
                    </div>
                    <div class="comment-body">
                        <h6 class="comment-title">
                            <span class="commenter-name"><?php comment_author(); ?></span>
                            <span class="comment-date"><?php esc_html_e("/","bigshow");?><?php comment_time('d M, Y'); ?></span>
                        </h6>
                        <div class="comment-content">
                            <?php comment_text(); ?>
                        </div>
                       
                       <?php comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'],'reply_text'=>'Reply' ) ) ); ?>
                    </div>

				</div>
		<?php
		}
	endif;

	/*
		function name: bigshow_social_link
		return type : string
	*/


	if(!function_exists('bigshow_social_link')){
		function bigshow_social_link($data=array()){
           
            $output = '';
            $social = array();
			
            if(!empty($data)){
                $social = $data;
            }
             
            $output .= '<ul class="chef-social">';
	            if(!empty($social) && is_array($social)) {
	                foreach ($social[0] as $link) {
	                 $link = strtolower($link);
	                    if (strpos($link, "facebook.com") !== false) {
	                    	$output .= '<li><a target="_blank" class="" href="' . esc_url($link) . '"><i class="fa fa-facebook"></i></a></li>';	
	                    } elseif (strpos($link, "twitter.com") !== false) {
	                        $output .= '<li><a class="" href="' . esc_url($link) . '"><i class="fa fa-twitter"></i></a></li>';
	                    }  elseif (strpos($link, "plus.google.com") !== false ) {
	                        $output .= '<li><a class="" href="' . esc_url($link) . '"><i class="fa fa-google-plus"></i></a></li>';
	                    } elseif (strpos($link, "youtube.com") !== false) {
	                        $output .= '<li><a  class="" href="' . esc_url($link) . '"><i class="fa fa-youtube"></i></a></li>';
	                    } 
	                
	                }
	            }
             $output .= '</ul>';
            return $output;
        }

    }
   
    
    /*===================*Menu*==========================*/
	
	if(!function_exists("bigshow_menu")){
		function bigshow_menu(){
			
	?>
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false">
						<span class="sr-only"><?php esc_html_e("Toggle navigation","bigshow"); ?></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					
					<a class="navbar-brand" href="<?php print esc_url(home_url('/')); ?>">
						<?php 
							$h_logo = get_theme_mod('header_logo');
							$h_logo =  (!empty($h_logo)) ? $h_logo : get_template_directory_uri().'/images/logo.png';
							print '<img alt="'.esc_attr__("image","bigshow").'" src="'.esc_url($h_logo).'">';
						?>
					</a>
				</div>
				<div class="collapse navbar-collapse" id="main-nav-collapse">
					<?php print bigshow_top_search_form(); ?>
					<?php

						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'	 => 'nav navbar-nav navbar-right',								   
							'fallback_cb' 	 => 'bigshow_primary_menu',
							'container'      => '',
							'walker'         => new bigshow_wp_bootstrap_navwalker(),
						) );
					?>
					
				</div>
			</div>
		</nav>
		
	<?php
			}
		}

	/*
		function name: bigshow_search_form
		return : form style
		
	*/
	
	if(!function_exists('bigshow_top_search_form')){	

		function bigshow_top_search_form( $form="" ) {
			
			if(BIGSHOW_PLUGIN_CUSTOM_POST){
				$taxonomy = 'movie_categories';
				$terms = get_terms($taxonomy);
				$res = "";
				
				$res .= '<form action="%s" method="get" class="navbar-form navbar-left">';
				if(!empty($terms)){
					$res .= '<select>';
					if ( $terms && !is_wp_error( $terms ) ) :
						foreach ($terms as $term) {
							$res .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
						}
					endif;
					$res .='</select>';
				}
					$res .='<input type="search" value="%s" required name="s" placeholder="%s">';
					$res .='<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>';
				$res .='</form>';

				$form = sprintf($res,esc_url( home_url('/')),esc_attr( get_search_query()),esc_attr__('Type and Hit Enter','bigshow'));
				return $form;
			}
		}
		
	}

	if(!function_exists('bigshow_search_form')){

		function bigshow_search_form( $form="" ) {
			
			$res = "";			
			$res .= '<form action="%s" method="get" class="navbar-form navbar-left">';
				$res .='<input type="search" value="%s" required name="s" placeholder="%s">';
				$res .='<button type="submit"><span><i class="fa fa-search" aria-hidden="true"></i></span></button>';
			$res .='</form>';

			$form = sprintf($res,esc_url( home_url('/')),esc_attr( get_search_query()),esc_attr__('Type and Hit Enter','bigshow'));
			return $form;
		}
		add_filter( 'get_search_form','bigshow_search_form');
		
	}
	
	if(!function_exists('bigshow_primary_menu')){
		function bigshow_primary_menu(){
			print '';
		}
	}
	if(!function_exists('bigshow_footer_menu')){
		function bigshow_footer_menu(){
			print '';
		}
	}
	
	/**
	* control setting
	*/
	
	if(!function_exists("bigshow_control")){
		function bigshow_control(){
			$loader = get_theme_mod('control_preloader');
	        $topbar = get_theme_mod('control_topbar');
	        $content = get_theme_mod('control_data');
	        $loader =  !empty( $loader) ?  $loader : "option1";
	        $topbar =  !empty( $topbar) ?  $topbar : "option1";
	        $content =  !empty( $content) ?   $content : 'Enjoy your favorite movie, TV show & video in a single subscription at $60';
	        if($loader == "option2"){
	        ?>
	            <div class="preloader" id="preloader">
	                <div class="lds-css ng-scope">
	                    <div class="lds-ripple">
	                        <div></div>
	                        <div></div>
	                    </div>
	                </div>
	            </div> 
	        <?php	}if( $topbar == "option2"){	?>
	        <header class="topbar text-white" id="topbar">
	            <div class="container">
	                <div class="row">
	                    <div class="col-lg-6 col-sm-8">
	                        <p class="topbar-intro"><?php print esc_html($content) ?></p>
	                    </div>
	                    <div class="col-lg-6 col-sm-4">
	                    <?php
	                    if(is_user_logged_in()){
	                    ?>
	                        <div class="topbar-right-btns">
	                            <a class="btn" href="<?php print esc_url(wp_logout_url( get_permalink())); ?>"><?php esc_html_e('Logout','bigshow'); ?></a>
	                            <a class="btn" href="<?php print esc_url(get_edit_user_link()); ?>"><?php esc_html_e('Profile','bigshow'); ?></a>
	                        </div>
	                    <?php }else{ ?>

	                        <div class="topbar-right-btns">
	                            <a class="btn" href="<?php print esc_url(wp_login_url( get_permalink() )); ?>"><?php esc_html_e('Login','bigshow'); ?></a>
	                            <a class="btn" href="<?php print esc_url(wp_registration_url()); ?>"><?php esc_html_e('Registation','bigshow'); ?></a>
	                        </div>
	                    <?php } ?>
	                    </div>
	                </div>
	            </div>
	        </header>
	        <?php }
		}
	}
	/*
		function name: bigshow latest movie
	*/
	
	if(!function_exists("bigshow_latest_movie")){
		function bigshow_latest_movie(){
	?>
			
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="section-header">
                        <h2 class="section-title"><?php esc_html_e("آخر الأفلام","bigshow"); ?></h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="owl-carousel video-carousel" id="video-carousel">
                    <?php
                        $query = new WP_Query(array('posts_per_page' => 10,'post_type' => 'movie'));
                        if($query->have_posts()):
	                        while($query->have_posts()) : $query->the_post();
	                        $movie_arr = array("movie_trailer",
	                                    "movie_rating",
										"movie_poster"										
	                                );
									
	                        foreach ($movie_arr as $key => $value) {
	                           $data = get_post_meta( get_the_ID(), $value, 1 );
	                           $movie_arr[$value]  = !empty( $data) ?  $data : ''; 
	                        }

	                        extract($movie_arr);
	                        $date = get_the_date('d F , Y');
                    ?>
                    <div class="video-item">
                        <?php if ( $movie_poster !='') {  $image = wp_get_attachment_image_src( $movie_poster, 'full' );?>
                            <div class="thumb-wrap">
								<img src="<?php print esc_url($movie_poster); ?>" alt="<?php esc_html_e('Movie Thumb','bigshow'); ?>">
                                <span class="rating"><?php print esc_html($movie_rating); ?></span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="<?php print esc_url($movie_trailer); ?>"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                        <?php }?>
                        <div class="video-details">
                            <h4 class="video-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <p class="video-release-on"><?php print esc_html($date); ?></p>
                        </div>
                    </div>
                <?php endwhile;  endif;?>
                </div>
            </div>
        </div>
<?php
		}

	}
