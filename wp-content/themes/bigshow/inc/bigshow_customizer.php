<?php
/**
 * Bigshow  Customizer functionality
 *
 * @package WordPress
 * @subpackage Bigshow 
 * @since Bigshow  1.0
 */

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since bigshow  1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function bigshow_customize_register($bigshow_customize){
//============== Panel (start)=================//
	//****************Header panel**************//
	$bigshow_customize->add_panel( 'header_part', array(
		'priority'       => 10,
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          =>  esc_html__('Bigshow Option ', 'bigshow'),		 
		) );
	
	
//=============== Panel (end) =====================//

//================ Section (start)=================//
	$bigshow_customize->add_section( 'bigshow_conterol' , array(
		'title'      =>  esc_html__( 'Bigshow Control', 'bigshow' ),
		'priority'   => 11,
		'panel'  => 'header_part',
		) );
	//*********** Header section ***************//
	$bigshow_customize->add_section( 'header_logo' , array(
		'title'      =>  esc_html__( 'Upload  Logo', 'bigshow' ),
		'priority'   => 11,
		'panel'  => 'header_part',
		) );
	//===============  footer section ===============//
	$bigshow_customize->add_section( 'footer_info' , array(
		'title'      =>  esc_html__( 'Footer Info', 'bigshow' ),
		'priority'   => 14,
		'panel'  => 'header_part',
		) );
	
	//=============== background ===============//
	$bigshow_customize->add_section( 'background' , array(
		'title'      =>  esc_html__( 'Background', 'bigshow' ),
		'priority'   => 16,
		'panel'  => 'header_part',
		) );
	$bigshow_customize->add_section( 'color-section' , array(
		'title'      =>  esc_html__( 'Color Option', 'bigshow' ),
		'priority'   => 17,
		'panel'  => 'header_part',
		) );
	//=============== Blog ===============//
	


//================ Section(end) =================//

	

//=========== Setting (start)===============//
	/*============== Control setting */
	$bigshow_customize->add_setting( 'control_topbar', array(
        'default'   => 'option1',
    ) );
    $bigshow_customize->add_setting( 'control_preloader', array(
        'default'   => 'option1',
    ) );
    $bigshow_customize->add_setting( 'apikey_data', array(
        'default'   => '',
		'transport'  => 'postMessage',
    ) );
     $bigshow_customize->add_setting( 'control_data' , array(
	    'default'     => '',
		'transport'  => 'postMessage',
	) );
	//*********** header ****************//


	$bigshow_customize->add_setting( 'header_logo' , array(
		'default'     => get_template_directory_uri().'/images/logo.png',
		'transport'  => 'postMessage',)
	);
	
	
	$bigshow_customize->add_setting( 'post_img' , array(
		'default'     => get_template_directory_uri().'/images/bg/page-header.png',
		'transport'  => 'postMessage',)
	);
	
    $bigshow_customize->add_setting( 'footer_data' , array(	    
		'transport'  => 'postMessage',
	) );
   /* ====================== Colour setting ====================== */

    $bigshow_customize->add_setting( 'top_color' , array(
		'default'     =>  '#ffffff',
		'transport'   => 'postMessage')
	);
	$bigshow_customize->add_setting( 'heading_color' , array(
		'default'     =>  '#505050',
		'transport'   => 'postMessage')
	);
	$bigshow_customize->add_setting( 'pra_color' , array(
		'default'     =>  '#505050',
		'transport'   => 'postMessage')
	);
	$bigshow_customize->add_setting( 'hover_color' , array(
		'default'     =>  '#eb1436',
		'transport'   => 'postMessage')
	);
	$bigshow_customize->add_setting( 'grid_first_hover_color' , array(
		'default'     =>  '#EB1436',
		'transport'   => 'postMessage')
	);
	$bigshow_customize->add_setting( 'grid_second_hover_color' , array(
		'default'     =>  '#7B4397',
		'transport'   => 'postMessage')
	);




//=========== Setting (End)===============//	



//================ Control(start) ==================//
	//*******************Control setting ****************//
	$bigshow_customize->add_control( 
		new WP_Customize_Control( 
			$bigshow_customize, 
			'control_topbar',
				array(
					'label'     => esc_html__( 'Topbar Title view', 'bigshow' ),
					'section'   => 'bigshow_conterol',
					'settings'  => 'control_topbar',
					'type'      => 'radio',
					'choices'   => array(
						'option1'   => 'off',
						'option2'   => 'on',
						),
				) 
		)
	);
	
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'control_data',
			array(
				'label'          => esc_html__( 'TopBar Content', 'bigshow' ),
				'section'        => 'bigshow_conterol',
				'settings'       => 'control_data',
				'type'           => 'textarea',
			)
		)
	);
	$bigshow_customize->add_control( 
		new WP_Customize_Control( 
			$bigshow_customize, 
			'control_preloader',
				array(
					'label'     => esc_html__( 'Preloader view', 'bigshow' ),
					'section'   => 'bigshow_conterol',
					'settings'  => 'control_preloader',
					'type'      => 'radio',
					'choices'   => array(
						'option1'   => 'off',
						'option2'   => 'on',
						),
				) 
		)
	);
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'apikey_data',
			array(
				'label'          => esc_html__( 'Map Api Key', 'bigshow' ),
				'section'        => 'bigshow_conterol',
				'settings'       => 'apikey_data',
				'type'           => 'text',
			)
		)
	);
	//**************** Header **********************//
	$bigshow_customize->add_control( 
		new WP_Customize_Image_Control(
			$bigshow_customize,
			'header_logo',
			array(
				'label'      =>  esc_html__( 'Upload a logo for Header', 'bigshow' ),
				'section'    => 'header_logo',
				'settings'   => 'header_logo',
				)
			)   );

	 	 
	$bigshow_customize->add_control( 
		new WP_Customize_Image_Control(
			$bigshow_customize,
			'post_img',
			array(
				'label'      =>  esc_html__( 'Upload Image For Sub Header', 'bigshow' ),
				'section'    => 'background',
				'settings'   => 'post_img',
				)
			)   
		);

	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'footer_data',
			array(
				'label'          => esc_html__( 'Footer Information', 'bigshow' ),
				'section'        => 'footer_info',
				'settings'       => 'footer_data',
				'type'           => 'textarea',
			)
		)
	);

	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'top_color',
			array(
				'label'          =>  esc_html__( 'Heading & footer  Color ', 'bigshow' ),
				'section'        => 'color-section',
				'settings'       => 'top_color',
				'description'    => 'Heading,banner, footer Text Color',
				'type'           => 'color',
				)
			)
		);
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'heading_color',
			array(
				'label'          =>  esc_html__( 'Content Heading  Color ', 'bigshow' ),
				'section'        => 'color-section',
				'settings'       => 'heading_color',
				'description'    => 'Heading & content Text Color',
				'type'           => 'color',
				)
			)
		);
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'pra_color',
			array(
				'label'          =>  esc_html__( 'Paragraph Color ', 'bigshow' ),
				'section'        => 'color-section',
				'settings'       => 'pra_color',
				'description'    => 'Blog  Text Color',
				'type'           => 'color',
				)
			)
		);
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'hover_color',
			array(
				'label'          =>  esc_html__( 'Hover Color ', 'bigshow' ),
				'section'        => 'color-section',
				'settings'       => 'hover_color',
				'description'    => 'Content Text Color',
				'type'           => 'color',
				)
			)
		);
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'grid_first_hover_color',
			array(
				'label'          =>  esc_html__( 'Gradient  Top Color ', 'bigshow' ),
				'section'        => 'color-section',
				'settings'       => 'grid_first_hover_color',
				'description'    => 'Gradient  Top Color',
				'type'           => 'color',
				)
			)
		);
	$bigshow_customize->add_control(
		new WP_Customize_Control(
			$bigshow_customize,
			'grid_second_hover_color',
			array(
				'label'          =>  esc_html__( 'Gradient  Bottom Color ', 'bigshow' ),
				'section'        => 'color-section',
				'settings'       => 'grid_second_hover_color',
				'description'    => 'Content Text Color',
				'type'           => 'color',
				)
			)
		);

//================ Control (end)==================//		

}
add_action('customize_register', 'bigshow_customize_register');