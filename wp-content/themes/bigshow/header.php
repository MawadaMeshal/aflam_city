<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 * @subpackage Bigshow
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <!-- Responsive Meta -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?>>

    <?php 
        bigshow_control();
        echo bigshow_menu();


    if(!is_front_page()) {
?>
    <div class="page-header">
        <div class="page-header-overlay">
            <div class="container">
           
            
            <?php  if(is_single()) {
                $post_date = get_the_date('d F Y');
                $date = explode(" ",$post_date);
                global $post;
                 $user_id=$post->post_author;
               // echo get_the_author_meta( 'display_name' , $user_id );exit;
                echo '<p class="header-metas">'.esc_html($date[0])." ".esc_html($date[1]).", ".esc_html($date[2])." | ".esc_html__("By - ","bigshow").get_the_author_meta( 'display_name' , $user_id ).' </p>';
            }

$term = get_queried_object() ; 

$taxonomy = $term->taxonomy ;

$sector = get_queried_object()->name;
            ?>
                <h2 class="page-title"><?php if($sector) {echo $sector;} else{ wp_title('');} ?></h2>
            </div>
        </div>
    </div>

   <?php }   ?> 
