<?php
/**
 * Bigshow functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Bigshow
 * @since Bigshow 1.0
 */

require_once('inc/postType.php');
 if ( ! function_exists( 'bigshow_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own bigshow_setup() function to override in a child theme.
 *
 * @since bigshow 1.0
 */

define( 'BIGSHOW_THEME_DIR', get_template_directory() );
define( 'BIGSHOW_THEME_URI', get_template_directory_uri() );
define( 'BIGSHOW_THEME_SUB_DIR', BIGSHOW_THEME_DIR.'/inc/' );
define( 'BIGSHOW_THEME_CSS_DIR', BIGSHOW_THEME_URI.'/css/' );
define( 'BIGSHOW_THEME_JS_DIR', BIGSHOW_THEME_URI.'/js/' );


/*
include Custom Function File
*/

require_once BIGSHOW_THEME_SUB_DIR.'bigshow_function.php';
require_once BIGSHOW_THEME_SUB_DIR.'bigshow_wp_bootstrap_navwalker.php';
require_once BIGSHOW_THEME_SUB_DIR.'class-tgm-plugin-activation.php';
require_once BIGSHOW_THEME_SUB_DIR.'bigshow_add_plugin.php';
require_once BIGSHOW_THEME_SUB_DIR.'bigshow_customizer.php';
if(!defined('BIGSHOW_PLUGIN_CUSTOM_POST')){
	define( 'BIGSHOW_PLUGIN_CUSTOM_POST', in_array( 'bigshow-custom-post/bigshow_custom_post.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
}

function bigshow_widgets_init() {

	register_sidebar( 
		array( 			
				'name' 			=> esc_html__( 'Right Sidebar', 'bigshow' ),
				'id' 			=> 'right-sidebar',
				'description' 	=> esc_html__( 'Widgets in this area will be shown on Right Sidebar.', 'bigshow' ),
				'before_title' 	=> '<h3 class="widget-title">',
				'after_title' 	=> '</h3>',
				'before_widget' => '<div class="right-side widget %2$s ">',
				'after_widget' 	=> '</div>'

                            
			));
	register_sidebar( 
		array( 			
			'name' 			=> esc_html__( 'Footer Sidebar', 'bigshow' ),
			'id' 			=> 'footer-sidebar',
			'description' 	=> esc_html__( 'Widgets in this area will be shown on Footer Sidebar.', 'bigshow' ),
			'before_title' 	=> '<h3  class="widget-title">',
			'after_title' 	=> '</h3>',
			'before_widget' => '<div class="col-md-4 col-xs-12 sm-bottom-40"><div class="widget %2$s ">',
			'after_widget' 	=> '</div></div>'
		)
	);
	

}
add_action( 'widgets_init', 'bigshow_widgets_init' );


define( 'BIGSHOW_THEME_NAME', 'bigshow' );
 
 
function bigshow_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Riana, use a find and replace
	 * to change 'bigshow' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'bigshow', get_template_directory() . '/languages' );
	
	//*set image size *//
	
	add_image_size('bigshow_movie_post_thumb',370,260,true);
	add_image_size('bigshow_home_post_thumb',170,160,true);
	add_image_size('bigshow_recent_thumb',70,70,true);

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	 // Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	

	/*
	 * Enable support for custom logo.
	 *
	 *  @since  Riana 1.1
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-background' );
	/*
	name: register_nav_menus
	return: menu area 
	*/
	
	register_nav_menus( array(
		'primary' =>  esc_html__( 'Primary Menu', 'bigshow' ),
		'footer-menu' =>  esc_html__( 'Footer Menu', 'bigshow' )
		
	) );
}
endif; // bigshow_setup
add_action( 'after_setup_theme', 'bigshow_setup' );


function bigshow_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bigshow_content_width', 900 );
}
add_action( 'after_setup_theme', 'bigshow_content_width', 0 );

/*
Name add_bigshow_script
@return add action with enqueue script

*/

function add_bigshow_script(){
	
	//add style
	wp_enqueue_style( 'bigshow',get_stylesheet_uri());
	wp_enqueue_style( 'bootstrap', 			    BIGSHOW_THEME_CSS_DIR . 'bootstrap.min.css', array() );
    wp_enqueue_style( 'bootstrap-rtl', 			    BIGSHOW_THEME_CSS_DIR . 'bootstrap-rtl.min.css', array() );
	wp_enqueue_style( 'font-awesome', 			BIGSHOW_THEME_CSS_DIR . 'font-awesome.min.css', array() );
	wp_enqueue_style( 'owl-carousel', 			BIGSHOW_THEME_CSS_DIR . 'owl.carousel.min.css', array() );
	wp_enqueue_style( 'owl-transitions', 		BIGSHOW_THEME_CSS_DIR . 'owl.transitions.css', array() );
	wp_enqueue_style( 'magnific-popup', 		BIGSHOW_THEME_CSS_DIR . 'magnific-popup.css', array() );
	wp_enqueue_style( 'bigshow-style',			BIGSHOW_THEME_CSS_DIR . 'main.css', array() );
	wp_enqueue_style( 'bigshow-responsive', 	BIGSHOW_THEME_CSS_DIR . 'responsive.css', array() );

	
	//add script
	
	wp_enqueue_script('bootstrap',				BIGSHOW_THEME_JS_DIR.'bootstrap.min.js',array('jquery'),false,true);
	wp_enqueue_script('owl.carousel',			BIGSHOW_THEME_JS_DIR.'owl.carousel.js',array('jquery'),false,true);
	wp_enqueue_script('magnific-popup',				BIGSHOW_THEME_JS_DIR.'jquery.magnific-popup.min.js',array('jquery'),false,true);
	wp_enqueue_script('bigshow-map',				BIGSHOW_THEME_JS_DIR.'map.js',array('jquery'),false,true);
	wp_enqueue_script('bigshow-custom',				BIGSHOW_THEME_JS_DIR.'custom.js',array('jquery'),false,false);

	//reply comments
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts','add_bigshow_script');

// /*-------------------------------------------*
//  *				Excerpt Length
//  *------------------------------------------*/

	function bigshow_excerpt($limit) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
		if (count($excerpt)>=$limit) {
			array_pop($excerpt);
			$excerpt = implode(" ",$excerpt);
		} else {
			$excerpt = implode(" ",$excerpt);
		}
		$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
		return $excerpt;
	}

	if(!function_exists('bigshow_new_excerpt_more')){
		function bigshow_new_excerpt_more( $more ) {
			return '';
		}
		add_filter('excerpt_more', 'bigshow_new_excerpt_more');
	}

	if(!function_exists('bigshow_custom_excerpt_length')){
		function bigshow_custom_excerpt_length($length) {
			global $post;
			if ($post->post_type == 'post'){
				return 25;
			}else{
				return 10;
			}
				
		}
		add_filter('excerpt_length', 'bigshow_custom_excerpt_length');
	}
	
	
	function bigshow_fonts_url() {
		$font_url = '';
		
		
		if ( 'off' !== _x( 'on', 'Google font: on or off', 'bigshow' ) ) {
			$font_url = add_query_arg( 'family', urlencode( 'Montserrat|Bowlby One|Quattrocento Sans:400,400italic,700italic,700&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
		}
		return $font_url;
	}
	
	function bigshow_scripts() {
		wp_enqueue_style( 'bigshow-fonts', bigshow_fonts_url(), array(), '1.0.0' );
	}
	add_action( 'wp_enqueue_scripts', 'bigshow_scripts' );

	if ( ! function_exists( 'bigshow_comment_nav' ) ) :
		function bigshow_comment_nav() {
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
			?>
			<nav class="navigation comment-navigation" role="navigation">
				<h2 class="screen-reader-text"><?php esc_html_e('Comment navigation', 'bigshow'); ?></h2>
				<div class="nav-links">
					<?php
						if ( $prev_link = get_previous_comments_link( esc_html__('Older Comments', 'bigshow') ) ) :
							printf( '<div class="nav-previous">%s</div>', $prev_link );
						endif;

						if ( $next_link = get_next_comments_link( esc_html__('Newer Comments', 'bigshow') ) ) :
							printf( '<div class="nav-next">%s</div>', $next_link );
						endif;
					?>
				</div><!-- .nav-links -->
			</nav><!-- .comment-navigation -->
			<?php
			endif;
		}
	endif;
	
	
	/*
    	Function Name   Blog background image 
		
		@Return  String
    */
	
	if(!function_exists('bigshow_background_image_load')):
		function bigshow_background_image_load() {
			$page_header = '';
			$movie_rating = '';
			$movie_rating2 = '';
			$page_header = get_theme_mod('post_img');
			$page_header =  (!empty($page_header)) ?  $page_header : get_template_directory_uri().'/images/bg/page-header.png';
			$movie_rating =  get_template_directory_uri().'/images/bstar.png';
			$movie_rating2 =  get_template_directory_uri().'/images/mstar.png';
			
			$custom_css = "
				.page-header {
					background-image: url($page_header);
				}
				.video-details .ratings-wrap .rating {
					background-image: url($movie_rating);
				}
				.wpb_wrapper .banner-slider-area .rating {
					background-image: url($movie_rating) !important;
				}
				.video-item .rating {
					background-image: url($movie_rating2);
				}";
			wp_add_inline_style( 'bigshow-style', $custom_css );
		}
		add_action( 'wp_enqueue_scripts', 'bigshow_background_image_load' );
	endif;
	if(!function_exists('bigshow_Hex2RGB')){
	
		function bigshow_Hex2RGB($color, $opacity=0.5){
			//echo $color; exit;
			
			$color = str_replace('#', '', $color);
			if (strlen($color) != 6){ return array(0,0,0); }
			$rgb = array();
			for ($x=0;$x<3;$x++){
				$rgb[$x] = hexdec(substr($color,(2*$x),2));
			}

			$output = 'rgba('.$rgb[0].','.$rgb[1].','.$rgb[2].','.$opacity.')';

			return $output;
		}
	}
	

	if(!function_exists('bigshow_color_setting')) {
		function bigshow_color_setting() {
			wp_enqueue_style('bigshow-load-style', BIGSHOW_THEME_CSS_DIR . 'bigshow-load-style.css');
			$top_color = '';
			$heading_color = '';
			$pra_color = '';
			$hover_color = '';
			$grid_first_hover_color = '';
			$grid_second_hover_color = '';
			
			$top_color 		= get_theme_mod('top_color');
			$heading_color 	= get_theme_mod('heading_color');
			$pra_color 	= get_theme_mod('pra_color');
			$hover_color 	= get_theme_mod('hover_color');
			$grid_first_hover_color 	= get_theme_mod('grid_first_hover_color');
			$grid_second_hover_color 	= get_theme_mod('grid_second_hover_color');

			$top_color = !empty($top_color) ? $top_color : "#ffffff";
			$heading_color = !empty($heading_color) ? $heading_color : "#505050";
			$pra_color = !empty($pra_color) ? $pra_color : "#111111";
			
			$hover_color = !empty($hover_color) ? $hover_color : "#eb1436";
			$hover_rgb_color = bigshow_Hex2RGB($hover_color,0.5);
			$grid_first_hover_color = !empty($grid_first_hover_color) ? $grid_first_hover_color : "#EB1436";
			$grid_first_rgb_color = bigshow_Hex2RGB($grid_first_hover_color,0.9);
			$grid_second_hover_color = !empty($grid_second_hover_color) ? $grid_second_hover_color : "#7B4397";
			$grid_second_rgb_color =  bigshow_Hex2RGB($grid_second_hover_color,0.9);
			
			$custom_css = "
				body,
				.news-featured .news-title,
				.news-featured .news-date-meta,
				.video-item .video-details .video-release-on,
				.contact-title, .contact-detail-title,
				.celebrity-item .celebrity-details .celebrity-profession,
				.video-single .content-wrap .video-details .rating
				{
					color: $heading_color;
				}
				.all-link,
				.video-item .rating{
					color: $heading_color !important;
				}
				
				.all-link:hover,
				.all-link:focus, 
				.all-link:visited,
				.news-item .news-date-meta,
				.video-item .video-details .video-title a:hover,
				.play-video,
				.video-item .thumb-hover .play-video i,
				.widget .widget-cat .cat a:hover,
				.navbar-default .navbar-nav > li.active a:hover,
				.navbar-default .navbar-nav > li.active a:focus, 
				.navbar-default .navbar-nav > li.active a:visited,
				.navbar-default .navbar-nav li > a:hover,
				.celebrity-item .celebrity-details .celebrity-name a:hover,
				.news-item .news-title a:hover,
				.news-entry blockquote:before,
				.post-tag-category a,
				.news-tag a,
				.comment-respond a,				
				.video-single .content-wrap .video-details .user-voted i,
				.single-gallery-slider .owl-prev:hover,
				.single-gallery-slider .owl-next:hover{
					color: $hover_color;
				}
				.play-video{
					box-shadow: 0 0 0 10px $hover_rgb_color;
				}
				.play-video:hover{
					box-shadow: 0 0 0 10px $hover_color;
				}
				.video-item .thumb-hover,
				.upcomming-item .upcomming-hover,
				.celebrity-item .thumb-hover{
					background-image: linear-gradient(to bottom, $grid_first_rgb_color 0%, $grid_second_rgb_color 100%);
				}
				.topbar,
				.news-featured .news-date-meta,
				.video-carousel .owl-prev:hover, 
				.video-carousel .owl-next:hover,
				.play-video,
				.btn,
				.news-list .news-link-btn,
				.comment-form [type=submit],
				.comment-body .comment-reply-link {
					background-color: $hover_color;
				}
				.text-white,
				.footer-logo + .about-text,
				.widget .widget-cat .cat a,
				.topbar-right-btns .btn,
				.text-white a, 
				.theme-bg a,
				.copyright-footer .copyright-text {
					color: $top_color;
				}
				.navbar-default .navbar-form select:hover,
				.navbar-default .navbar-form select:focus,
				.navbar-default .navbar-form [type=search]:focus,
				.video-carousel .owl-prev:hover, 
				.video-carousel .owl-next:hover{
					border-color: $hover_color;
				}
				.navbar-default .navbar-form [type=submit]:hover, 
				.navbar-default .navbar-form [type=submit]:focus{
					border-color: $hover_color;
					color: $hover_color;
				}
				.topbar-right-btns .btn:hover{
					color: $hover_color;
    				background-color: $top_color;
				}
				.news-item .news-title,
				.video-item .video-details .video-title a,
				.celebrity-item .celebrity-details .celebrity-name{
					color: $pra_color;
				}
				.banner-slider .owl-page.active{
					background-color: $hover_rgb_color;
				}

				.news-entry blockquote{
					border-color: #e8e8e8 #e8e8e8 #e8e8e8 $hover_color;
				}

				";
			
			
			wp_add_inline_style( 'bigshow-load-style', $custom_css );

		}	
		add_action( 'wp_enqueue_scripts', 'bigshow_color_setting' );
	}


/* america icon*/

add_action( 'add_meta_boxes', 'match_box' );

function match_box() {

    add_meta_box(

        'match_id',

        __( 'معلومات اخرى'),

        'metamatch',

        'matches'

    );

}



function metamatch( $post) {

    ?>

    <input type="hidden" name="am" value="am">

    <p><label> <span style="display:inline-block; width:220px;"> <?php echo "first team"; ?>   </span>

            <input type="text" name="first_team" id="first_team" style="width:300px;" value="<?php echo get_post_meta( $post->ID,'first_team', true );?>" />

        </label></p>

    <p><label> <span style="display:inline-block; width:220px;"> <?php echo "second team"; ?>   </span>

            <input type="text" name="second_team" id="second_team" style="width:300px;" value="<?php echo get_post_meta( $post->ID,'second_team', true );?>" />

        </label></p>

    <p><label> <span style="display:inline-block; width:220px;"> <?php echo "commentator"; ?>   </span>

            <input type="text" name="commentator" id="commentator" style="width:300px;" value="<?php echo get_post_meta( $post->ID,'commentator', true );?>" />

        </label></p>

    <p><label> <span style="display:inline-block; width:220px;"> <?php echo "football stadium"; ?>   </span>

            <input type="text" name="football_stadium" id="football_stadium" style="width:300px;" value="<?php echo get_post_meta( $post->ID,'football_stadium', true );?>" />

        </label></p>

    <p><label> <span style="display:inline-block; width:220px;"> <?php echo "time"; ?>   </span>

            <input type="text" name="time" id="time" style="width:300px;" value="<?php echo get_post_meta( $post->ID,'time', true );?>" />

        </label></p>

    <p><label> <span style="display:inline-block; width:220px;"> <?php echo "url"; ?>   </span>

            <input type="text" name="url" id="url" style="width:300px;" value="<?php echo get_post_meta( $post->ID,'url', true );?>" />

        </label></p>






    <?php

}

function save_match( $post_id ,$post) {



    if (isset($_POST['am']) && $_POST['am']=='am') {

        update_post_meta( $post_id, 'first_team', $_POST['first_team'] );
        update_post_meta( $post_id, 'second_team', $_POST['second_team'] );
        update_post_meta( $post_id, 'commentator', $_POST['commentator'] );
        update_post_meta( $post_id, 'football_stadium', $_POST['football_stadium'] );
        update_post_meta( $post_id, 'time', $_POST['time'] );
        update_post_meta( $post_id, 'url', $_POST['url'] );




    }

}

add_action( 'save_post', 'save_match' , 10, 2);
