<?php
	$post_date = get_the_date('d F Y');
	$date = explode(" ",$post_date);
    if(is_single()) {
?>
	
	<div class="news-single-main">
		<div class="single-thumb">
			<?php
				if ( has_post_thumbnail()) {
					the_post_thumbnail('full', array('class' => 'img-responsive'));
				}
			?>
		</div>
		<div class="news-entry">
			<?php the_content(); ?>
		</div>
		<?php 
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:','bigshow' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text"></span>%',
				
			) );
		?>
	</div>

	<!-- News Footer -->
   <div class="single-footer">
		<div class="row">
			<div class="col-sm-12">
				<div class="post-tag-category">
					<?php 
						print '<label>'.esc_html__("Category: ","bigshow").' </label>';
						the_category(' ');
					?>
				</div>
			</div>
		</div>
		<div class="row">
			<?php if ( shortcode_exists( 'cp_post_share' ) ) { ?>
					<div class="col-md-6 col-xs-12">
						<div class="news-share">
							 <label><?php esc_html_e("Share: ","bigshow"); ?></label>
							<?php echo do_shortcode('[cp_post_share]'); ?>
						</div>
					</div>
			<?php } ?>
			<?php if(has_tag()){ ?>
				<div class="col-md-6 col-xs-12">
					<div class="news-tag">
						<?php 
							print '<label>'.esc_html__("Tags: ","bigshow").' </label>';
							the_tags(" ", ", ");
						?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
	
	<?php } else { ?>
	
        
		<div <?php post_class('news-item'); ?> id="post-<?php the_ID(); ?>">
            <div class="row">
				<?php 
					$post_grid = 'col-md-6 col-xs-12';
					if(!has_post_thumbnail()){
						$post_grid = 'col-md-12';
					}
				?>
                <?php if(has_post_thumbnail()){ ?>
					<div class="col-md-6 col-xs-12">
						<a class="news-thumb" href="<?php the_permalink(); ?>">
						<?php 
						$post_poster = get_post_meta( get_the_ID(), 'post_poster', 1 );
						if ( !empty($post_poster)) { ?>										    
				                <img src="<?php print esc_url($post_poster); ?>" alt="<?php esc_attr_e('Movie Thumb','bigshow'); ?>">
				        <?php } else { the_post_thumbnail('full', array('class' => 'img-responsive')); } ?>
						</a>
					</div>
                <?php } ?>
				
                <div class="<?php print esc_attr($post_grid); ?>">
                    <div class="news-content">
                        <p class="news-metas">
                            <span class="news-meta date-meta"><?php print $date[0]." ".$date[1].", ".$date[2]; ?> </span> | <span class="news-meta author-meta"><?php esc_html_e("By -","bigshow"); ?> <a href="<?php get_the_author_link();?>"><?php print get_the_author(); ?></a></span>
                        </p>
                        <h3 class="news-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p class="news-excerpt"><?php print bigshow_excerpt(16); ?></p>
                        <a class="news-link-btn" href="<?php the_permalink(); ?>"><?php  esc_html_e('Read more...','bigshow'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
