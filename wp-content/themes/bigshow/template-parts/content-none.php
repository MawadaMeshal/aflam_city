<div class="main-wrap">
    <div class="section section-padding news-list-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-not-found">
                        <h3>
                            <li><?php esc_html_e("4","bigshow")?></li>
                            <li><?php esc_html_e("0","bigshow")?></li>
                            <li><?php esc_html_e("4","bigshow")?></li>
                        </h3>
                        <h4><?php esc_html_e("Page Not Found","bigshow")?></h4>
                        <p><?php esc_html_e("Go back to Homepage","bigshow")?></p>
                        <a href="<?php echo esc_url(home_url('/')); ?>" class="btn btn-primary"><?php esc_html_e("Homepage","bigshow")?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>