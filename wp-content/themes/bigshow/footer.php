<?php
/**
 * The template for displaying the footer
 * @package WordPress
 * @subpackage Bigshow
 */
?>
        <footer class="text-white">
            <?php if(is_active_sidebar('footer-sidebar')) { ?>
                <div class="footer-widget-area">
                    <div class="container">
                        <div class="row">
                            <?php dynamic_sidebar('footer-sidebar'); ?>
                        </div>
                    </div>
                </div>
            <?php } 
                $footer_data = get_theme_mod("footer_data"); 
                $footer_data = !empty($footer_data) ? $footer_data :'@ 2017 BigShow. All Rights Reserved'; 
            ?>
            <div class="copyright-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-sm-7 col-xs-12 xs-text-center">
                            <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'footer-menu',
                                    'menu_class'     => 'footer-nav',                                 
                                    'fallback_cb'    => 'bigshow_footer_menu',
                                    'container'      => '',
                                    'walker'         => new bigshow_wp_bootstrap_navwalker(),
                                ) );
                            ?>
                        </div>
                        <div class="col-md-2 col-sm-5 col-xs-12 xs-text-center">
                            <p class="copyright-text"><?php echo esc_html($footer_data); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    <?php wp_footer(); ?>
    </body>
</html>

