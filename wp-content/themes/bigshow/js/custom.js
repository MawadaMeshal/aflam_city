

(function ($) {
    $(document).ready(function () {
        'use strict';


		/*-----------------------------------------
		Mobile dropdown toggle
		-----------------------------------------*/
		function dropdownToggle() {
			if ($(window).width() < 992) {
				$('.navbar-toggle').css('display', 'block');
				$('.navbar-collapse').css('display', 'none');

				$('.dropdown').unbind('click');

				$('.dropdown').on('click', function (dd) {
					dd.stopPropagation();
					$(this).children('.dropdown-menu').slideToggle();
				});
			} else {
				$('.navbar-toggle').css('display', 'none');
				$('.navbar-collapse').css('display', 'block');
			}
		}

		dropdownToggle();

		/*-----------------------------------------
		Header Slider
		-----------------------------------------*/
		$(window).on('load', function(){
			$('#banner-slider').owlCarousel({
				rtl:true,
                loop:true,
                margin:0,
                items: 1,
                autoplay:true,
                autoplayTimeout:4000,
                autoplayHoverPause:false,
                smartSpeed:450,
                center: true,
                responsiveClass:true,
                dots: true,
                nav: true,
                navText: ["<i class='fa fa-chevron-right fa-3x'></i>","<i class='fa fa-chevron-left fa-3x'></i>"],
                navClass: ['owl-prev' ,'owl-next'],
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        loop:true
                    },
                    600:{
                        items:1,
                        nav:true,
                        loop:true
                    },
                    1000:{
                        items:1,
                        nav:true,
                        loop:true
                    }
                }
			})
		});

		/*-----------------------------------------
		Video Carousel
		-----------------------------------------*/

		$('.video-carousel').owlCarousel({

            rtl:true,
            loop:true,
            margin:0,
            items: 4,
            autoplay:true,
            autoplayTimeout:4000,
            autoplayHoverPause:false,
            smartSpeed:450,
            center: true,
            stagePadding: 0,
            responsiveClass:true,
            dots: true,
            nav: true,
            navText: ["<i class='fa fa-chevron-right fa-3x'></i>","<i class='fa fa-chevron-left fa-3x'></i>"],
            navClass: ['owl-prev' ,'owl-next'],
            responsive:{
                0:{
                    items:1,
                    nav:false,
                    loop:true
                },
                600:{
                    items:1,
                    nav:true,
                    loop:true
                },
                1000:{
                    items:4,
                    nav:true,
                    loop:true
                }
            }
        });


        /*-----------------------------------------
		Single Gallery Slider
		-----------------------------------------*/
        $('.single-gallery-slider').owlCarousel({
            singleItem: true,
            slideSpeed: 200,
            autoPlay: 3000,
            stopOnHover: true,
            navigation: true,
            navigationText: ['<i class=\"fa fa-angle-left\"></i>', '<i class=\"fa fa-angle-right\"></i>'],
            pagination: false,
        });

        /*-----------------------------------------
        Magnific Popup
        -----------------------------------------*/
        $('.image-large').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
        $('.play-video-single').magnificPopup({
            type: 'iframe'
        });
        $.extend(true, $.magnificPopup.defaults, {
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: 'http://www.youtube.com/embed/%id%?autoplay=1'
                    }
                }
            }
        });
        
		/*-----------------------------------------
		All window event
		-----------------------------------------*/
		$(window).on('resize orientationchange', function () {
			dropdownToggle();
		});
	});


	/*-----------------------------------------
	Preloader
	-----------------------------------------*/
	$(window).on('load', function () {
		$('#preloader').fadeOut('slow');
	});
})(jQuery);
