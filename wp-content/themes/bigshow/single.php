<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage Bigshow
 */
 get_header();
 ?>

<div class="main-wrap">
    <div class="section section-padding news-list-section">
        <div class="container">
            <div class="row">
            	<div class="col-md-8 col-sm-12">
                    <div class="news-single">
                    	<?php
    						if ( have_posts() ) :
    							while ( have_posts() ) : the_post();
    								get_template_part( 'template-parts/content', get_post_format());
    							endwhile;
    						else:
    							get_template_part( 'template-parts/content', 'none' );
    						endif;
                             if(comments_open() || get_comments_number()) {
                                comments_template();
                            }
    					?>
                    </div>
            	</div>
            	<div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-12">
                    
                    <?php get_sidebar(); ?>	
            		
            	</div>
            </div>
        </div>
    </div>
</div>
	
<?php get_footer(); ?>
