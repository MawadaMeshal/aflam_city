<?php
/**
* The template for displaying pages
* @package WordPress
* @subpackage bigshow
*
*/
	get_header();
	
?>
	<div class="section-padding">
		<div class="container">
			<div class="page-content">
			   <?php 
				 if(have_posts()){
						while ( have_posts() ): the_post();
							the_content();
					endwhile;
					if ( comments_open() || get_comments_number() ) {
					?>
						<div class="blog-post single-post">
							<div class="comment-section">
								<?php comments_template(); ?>
							</div>
						</div>
					<?php }
				 } else {
						 get_template_part( 'template-parts/content', 'none' );
				 }
				?>               
			</div>
		</div>
	</div>

<?php get_footer();
