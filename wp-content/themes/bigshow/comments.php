<?php 
/**
* The template for displaying comments
*
* @package WordPress
* @subpackage bigshow
*/

?>
<div class="given-comment">
    <h3 class="given-comment-title">
	    <?php 
			$comment_no = number_format_i18n(get_comments_number());
			$comment_no = (!empty($comment_no))  ? '<h2>'.$comment_no .' Comments</h2>': ' ';
			echo sprintf("%s", $comment_no);
		?>
		
	</h3>
    <ul class="comments">
       <?php
			wp_list_comments( array(
				'style'       => 'ul',
				'short_ping'  => true,
				'avatar_size' => 80,
				'callback'    => 'bigshow_comment',
			) );
		?>
    </ul>
</div>
 <div class="commenting-wrap">   
    <?php
		$commenter = wp_get_current_commenter();
		$req       = get_option( 'require_name_email' );
		$aria_req  = ( $req ? " aria-required='true'" : '' );
		$required_text = '  ';
		$args     = array(
			'class_form'        =>'comment-form',			
			'title_reply'       => esc_html__('Leave Your Comments','bigshow'),
			'submit_button'       => '<button type="submit">'.esc_attr__('Comment','bigshow').'</button>',
			'comment_field' 	=>  '<textarea name="comment" '.$aria_req.' id="comment"  placeholder="'.esc_html__('comment','bigshow').'" rows="10"></textarea>',
			'fields' => apply_filters( 'comment_form_default_fields',
				array(
					'author'   => '<div class="col-md-6"><input name="author" id="name" type="text" placeholder="'.esc_html__('Name','bigshow').'"></div>',
					'email'    => '<div class="col-md-6"><input name="email" id="mail" type="text" placeholder="'.esc_html__('Email','bigshow').'"></div>',
					
				) ),
			'label_submit' => esc_html__('Comment','bigshow'),
			);
		comment_form($args);
	?>
</div>