<?php
/**
 * @category     WordPress_Plugin
 * @package      bigshow Custom Post
 * @author       Codepassenger
 * Plugin Name:  bigshow Custom Post
 * Author:       Codepassenger
 * Author URI:   http://Codepassenger.com
 * Text Domain:  bigshow
 * **********************************************************************
 */

//============ Custom Post Practice ================//
define( 'BIGSHOW_METABOX', in_array( 'cmb2/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );

if(BIGSHOW_METABOX ){
	require_once plugin_dir_path( __FILE__ )."bigshow_metabox.php";
	require_once plugin_dir_path( __FILE__ ).'cmb-field-select2.php';
	
	function cmv2_script(){
		wp_enqueue_style( 'style-css', 				plugin_dir_url( __FILE__ ) . 'css/style.css', array() );
		wp_enqueue_style( 'select2-css', 	plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array() );
		wp_enqueue_script('bscript',			plugin_dir_url( __FILE__ ) . 'js/script.js',array('jquery'),false,true);
		wp_enqueue_script('select2-js',			plugin_dir_url( __FILE__ ) . 'js/select2.min.js',array('jquery'),false,true);

	}
	add_action('wp_enqueue_scripts','cmv2_script');
}

/*
	function name: bigshow_movie_post_type
*/
	if(!function_exists('bigshow_movie_post_type')):
		function bigshow_movie_post_type(){
			$labels = array( 
				'name'                	=> _x( 'Movie', 'Movie', 'bigshow' ),
				'singular_name'       	=> _x( 'Movie', 'Movie', 'bigshow' ),
				'menu_name'           	=> esc_html__( 'Movie', 'bigshow' ),
				'parent_item_colon'   	=> esc_html__( 'Movie:', 'bigshow' ),
				'all_items'           	=> esc_html__( 'All Movie ', 'bigshow' ),
				'view_item'           	=> esc_html__( 'View All ', 'bigshow' ),
				'add_new_item'        	=> esc_html__( 'Add New Movie', 'bigshow' ),
				'add_new'             	=> esc_html__( 'Add New Movie', 'bigshow' ),
				'edit_item'           	=> esc_html__( 'Edit ', 'bigshow' ),
				'update_item'         	=> esc_html__( 'Update ', 'bigshow' ),
				'search_items'        	=> esc_html__( 'Search ', 'bigshow' ),
				'not_found'           	=> esc_html__( 'No article found', 'bigshow' ),
				'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'bigshow' ),

			);
			
			$args = array(
				'labels'             	=> $labels,
				'public'             	=> true,
				'show_in_menu'       	=> true,
				'show_in_admin_bar'   	=> true,
				'can_export'          	=> true,
				'has_archive'        	=> true,
				'menu_icon'             => 'dashicons-video-alt',
				'supports'           	=> array( 'title','editor','thumbnail','gallery',),
				
				

			);
			register_post_type('movie',$args);
		}
		add_action('init','bigshow_movie_post_type');
		function themes_taxonomy() {  
	   
	   $labels = array(
		    'name' => _x( 'Movie', 'bigshow' ),
		    'singular_name' => _x( 'movie_categories', 'bigshow' ),
		    'search_items' =>  __( 'Search Movie ' ),
		    'all_items' => __( 'All Movie' ),
		    'parent_item' => __( 'Parent category' ),
		    'parent_item_colon' => __( 'Parent Topic:' ),
		    'edit_item' => __( 'Edit category' ), 
		    'update_item' => __( 'Update category' ),
		    'add_new_item' => __( 'Add New category' ),
		    'new_item_name' => __( 'New category Name' ),
		    'menu_name' => __( 'Movie Category' ),
		  ); 

	    register_taxonomy(  
	        'movie_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
	        'movie',        //post type name
	        array( 
	            'label' => 'Movie Category',  //Display name
	            'labels' => $labels,
	        ) 
	    );  
	}  
	add_action( 'init', 'themes_taxonomy');

	endif;

	if(!function_exists('bigshow_celebrity_post_type')):
		function bigshow_celebrity_post_type(){
			$labels = array( 
				'name'                	=> _x( 'Celebrity', 'Celebrity', 'bigshow' ),
				'singular_name'       	=> _x( 'Celebrity', 'Celebrity', 'bigshow' ),
				'menu_name'           	=> esc_html__( 'Celebrity', 'bigshow' ),
				'parent_item_colon'   	=> esc_html__( 'Celebrity:', 'bigshow' ),
				'all_items'           	=> esc_html__( 'All Celebrity ', 'bigshow' ),
				'view_item'           	=> esc_html__( 'View All ', 'bigshow' ),
				'add_new_item'        	=> esc_html__( 'Add New Celebrity', 'bigshow' ),
				'add_new'             	=> esc_html__( 'Add New Celebrity', 'bigshow' ),
				'edit_item'           	=> esc_html__( 'Edit ', 'bigshow' ),
				'update_item'         	=> esc_html__( 'Update ', 'bigshow' ),
				'search_items'        	=> esc_html__( 'Search ', 'bigshow' ),
				'not_found'           	=> esc_html__( 'No article found', 'bigshow' ),
				'not_found_in_trash'  	=> esc_html__( 'No article found in Trash', 'bigshow' )
			);
			
			$args = array(
				'labels'             	=> $labels,
				'public'             	=> true,
				'publicly_queryable' 	=> true,
				'show_in_menu'       	=> true,
				'show_in_admin_bar'   	=> true,
				'can_export'          	=> true,
				'has_archive'        	=> false,
				'hierarchical'       	=> false,
				'menu_icon'             => 'dashicons-awards',
				'supports'           	=> array( 'title','editor','thumbnail'),
				

			);
			register_post_type('celebrity',$args);
		}
		add_action('init','bigshow_celebrity_post_type');
	endif;
	

