<?php

add_filter( 'rwmb_meta_boxes', 'bigshow_movie_data' );

function bigshow_movie_data(array $field){
	$field[] = array(
		'id'			=>'movie',
		'title'			=>  esc_html__('Movie Information','bigshow'),
		'object_types'	=>array('movie'),
		'fields' 		=> array(
			array(
				'name' 	=>  esc_html__('Trailer Url','bigshow'),
				'type' => 'text_url',
				'id' 	=> 'movie_trailer',					
				'title'	=> esc_html__('Please Give Movie Trailer Url','bigshow'),					
			),
			array(
				'name' 	=>  esc_html__("Movie's Poster","bigshow"),
				'type' => 'file',
				'id' 	=> 'movie_poster',					
				'title'	=> esc_html__("Please Give Movie's Poster Image","bigshow"),
				'desc'    => esc_html__('Image Size Must Be 270 X 390 px','bighsow')					
			),
			array(
				'name' 	=>  esc_html__('Movie Rating','bigshow'),
				'type' 	=> 'text',
				'id' 	=> 'movie_rating',					
				'title'	=> esc_html__('Give Movie Total Rating','bigshow'),
				
			),
			array(
				'name' 	=>  esc_html__('Cast & Crew','bigshow'),
				'type' 	=> 'text',
				'id' 	=> 'movie_cust',					
				'title'	=> esc_html__('Give Movie Cast And Crew Name','bigshow'),				
			),
			array(
				'name' 	=>  esc_html__('Movie Duration','bigshow'),
				'type' 	=> 'text',
				'id' 	=> 'movie_duration',					
				'title'	=> esc_html__('Give Full Movie Duration','bigshow'),				
			),
			array(
				'name' 	=>  esc_html__('Movie Genres','bigshow'),
				'type' 	=> 'text',
				'id' 	=> 'movie_genres',					
				'title'	=> esc_html__('Give Movie Genres','bigshow'),				
			),
			array(
				'name' 	=>  esc_html__('Movie Country','bigshow'),
				'type' 	=> 'text',
				'id' 	=> 'movie_country',					
				'title'	=> esc_html__('Give Movie Industry Name','bigshow'),				
			),
			array(
				'name' 	=>  esc_html__('Movie Language','bigshow'),
				'type' 	=> 'text',
				'id' 	=> 'movie_language',					
				'title'	=> esc_html__('Give Movie Language','bigshow'),				
			),
			array(
				'name' 	=>  esc_html__('Gallery','bigshow'),
				'type' => 'file_list',
				'id' 	=> 'movie_gallery',
				'description' 	=> esc_html__('Give movie Gallery image(image size 870x450px)','bigshow'),
			),
		)
	);
	
	return $field;
}
	add_filter('cmb2_meta_boxes','bigshow_movie_data');
	
	
	
	function bigshow_post_data(array $field){
		$field[] = array(
			'id'			=>'post_data',
			'title'			=>  esc_html__('Post Thumb','bigshow'),
			'object_types'	=>array('post'),
			'fields' 		=> array(
				array(
					'name' 	=>  esc_html__("Post Poster","bigshow"),
					'type' => 'file',
					'id' 	=> 'post_poster',					
					'title'	=> esc_html__("Please Give Movie's Poster Image","bigshow"),
					'desc'    => esc_html__('Image Size Must Be 270 X 390 px','bighsow')					
				),
			)
		);
		
		return $field;
	}				
	
	add_filter('cmb2_meta_boxes','bigshow_post_data');	
	
	
	function bigshow_celebrity_data(array $field){
		$field[] = array(
			'id'			=>'celebrity_data',
			'title'			=>  esc_html__('Celebrity Information','bigshow'),
			'object_types'	=>array('celebrity'),
			'fields' 		=> array(
				array(
					'name' 	=>  esc_html__('Crew Type','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_type',		
					'title'	=> esc_html__('Crew Type','bigshow'),			
				),
				array(
					'name' 	=>  esc_html__('Birth Name:','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_name',			
					'title'	=> esc_html__('Birth Name:','bigshow'),
					
				),
				array(
					'name' 	=>  esc_html__('Date of Birth :','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_dob',			
					'title'	=> esc_html__('Date of Birth:','bigshow'),
				),
				array(
					'name' 	=>  esc_html__('Residence :','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_residence',			
					'title'	=> esc_html__('Residence:','bigshow'),
				),
				
				array(
					'name' 	=>  esc_html__('Country (Nationality):','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_country',			
					'title'	=> esc_html__('Country:','bigshow'),
				),
				array(
					'name' 	=>  esc_html__('Gender :','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_gender',			
					'title'	=> esc_html__('Gender:','bigshow'),
				),
				array(
					'name' 	=>  esc_html__('Language :','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_language',			
					'title'	=> esc_html__('Language:','bigshow'),
				),array(
					'name' 	=>  esc_html__('Height :','bigshow'),
					'type' 	=> 'text',
					'id' 	=> 'crew_height',			
					'title'	=> esc_html__('height:','bigshow'),
				),
				array(
				'name' 	=>  esc_html__('Gallery','bigshow'),
				'type' => 'file_list',
				'id' 	=> 'crews_gallery',
				'description' 	=> esc_html__('Give movie Gallery image(image size 870x450px)','bigshow'),
				),
				array(
					'name'    => 'Movie Name',
					'id'      => 'crews_movie',
					'description'    => 'Select Movie From List',
					'type'    => 'pw_multiselect',
					'options' => movie_list(),					
					
				)

			)
		);
		
		return $field;
	}
	add_filter('cmb2_meta_boxes','bigshow_celebrity_data');

	function movie_list(){
		$query = new WP_Query(array('posts_per_page' => -1,'post_type' => 'movie'));
        $movie_name = array();
        if($query->have_posts()):
	       while($query->have_posts()) : $query->the_post();

	       		$id = get_the_id();
	       		$name = get_the_title();
	       		$movie_name[$id] = $name;
	       endwhile;

       endif;
       return $movie_name;
	}

	