<?php
add_shortcode( 'bigshow_movie', function($atts, $content = null) {
	
	extract(shortcode_atts(array(		
		'title'			=>'',
		'post_num'		=>'',
		'cat'			=>'',
		'class'			=>'',
		'style'			=>'',
		'link'			=>'',
		),$atts));
		global $post;
		$style = !empty($style) ? $style:1;
		$output = '';			
	
		if($style ==1){
			$output.='<div class="show-listing">';
				$query = new WP_Query(array('posts_per_page' => $post_num,'post_type' => 'movie'));
				if($query->have_posts()):
					while($query->have_posts()) : $query->the_post();
						$movie_arr =array("movie_trailer",
									"movie_rating",
									 "movie_poster"
						);
						 foreach ($movie_arr as $key => $value) {
						   $data = get_post_meta( get_the_ID(), $value, 1 );
						   echo $movie_arr[$value]  = !empty( $data) ?  $data : ''; 
						}
						
						extract($movie_arr);
						$date = get_the_date('d F , Y');
						
						$output.='<div class="col-md-3 col-sm-4 col-xs-6">';
							$output.='<div class="video-item">';
								if ( $movie_poster != '') {
									$output.='<div class="thumb-wrap">';
										$output.='<img src="'. esc_html($movie_poster).'" alt="Movie Thumb">';
										$output.='<span class="rating">'.esc_html($movie_rating).'</span>';
										$output.='<div class="thumb-hover">';
											$output.='<a class="play-video" href="'.esc_url(get_the_permalink()).'"><i class="fa fa-play"></i></a>';
										$output.='</div>';
									$output.='</div>';
								}
								$output.='<div class="video-details">';
									$output.='<h4 class="video-title"><a href="'.esc_url(get_the_permalink()).'">'.get_the_title().'</a></h4>';
									$output.='<p class="video-release-on">'.esc_html($date).'</p>';
							   $output.=' </div>';
							$output.='</div>';
						$output.=' </div>';
					endwhile;
					wp_reset_query();
				endif;
			$output.='<div>';
		}else{
			$output.='<div class="row">';
                $output.='<div class="col-sm-8 col-xs-6">';
                    $output.='<div class="section-header">';
                        $output.='<h2 class="section-title">'.esc_html($title).'</h2>';
                    $output.='</div>';
                $output.='</div>';
                $output.='<div class="col-sm-4 col-xs-6">';
                    $output.='<a class="all-link" href="'.esc_url($link).'">'.esc_html__("مشاهدة المزيد","bighsow").'</a>';
                $output.='</div>';
            $output.='</div>';

            $output.='<div class="row">';
	            $output.='<div class="owl-carousel video-carousel" >';
					$query = new WP_Query(array('posts_per_page' => $post_num,'post_type' => 'movie','category_name' =>$cat));
	                if($query->have_posts()):
					
	                	while($query->have_posts()) : $query->the_post();
						
							$movie_arr =array("movie_trailer",
							            "movie_rating",
										"movie_poster"
							);
							 foreach ($movie_arr as $key => $value) {
							   $data = get_post_meta( get_the_ID(), $value, 1 );
							   $movie_arr[$value]  = !empty( $data) ?  $data : ''; 
							}

							extract($movie_arr);
							$date = get_the_date('d F , Y');
			                $output.='<div class="video-item">';

								if ( $movie_poster !='') {
							
				                   $output.=' <div class="thumb-wrap">';
				                        $output.='<img src="'.esc_html($movie_poster).'" alt="Movie Thumb">';
				                        $output.='<span class="rating">'.esc_html($movie_rating).'</span>';
				                        $output.='<div class="thumb-hover">';
				                            $output.='<a class="play-video" href="'.esc_url(get_the_permalink()).'"><i class="fa fa-play"></i></a>';
				                        $output.='</div>';
				                    $output.='</div>';
				                }
			                    $output.='<div class="video-details">';
			                        $output.='<h4 class="video-title"><a href="'.esc_url(get_the_permalink()).'">'.get_the_title().'</a></h4>';
			                        $output.='<p class="video-release-on">'.esc_html($date).'</p>';
			                    $output.='</div>';
			                $output.='</div>';
			            endwhile;
			            wp_reset_query() ;
			        endif;	                
	           $output.=' </div>';
	        $output.='</div>';
		}
		return $output;
});
if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Movie","bigshow"),
		"base"			=> "bigshow_movie",
		"class"			=> "",
		"description"	=>esc_html__("Type Movie Post Number To Show","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(

			array(
				"type"			=>"dropdown",
				"heading"		=> esc_html__("post Style","bigshow"),
				"param_name"	=> "style",				
				"value"			=> array("ALL"=>1,"Select Category"=>2),
				'description' 	=> esc_html__( 'Select Style From Dropdown(	"All"= Show All Movie , Category="Show post accordingly Category "', 'bigshow' ),	
			),
			array(
				"type"		=>"dropdown",
				"heading"	=> esc_html__("category","bigshow"),
				"param_name"=> "cat",
				"value"			=> tag_show(),
				'description' => esc_html__( 'Movie Category.', 'bigshow' ),	
				"dependency" => array(
			        "element" => "style",
			        "value" => "2"
			    ),
			),
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("Title","bigshow"),
				"param_name"	=> "title",	
				'description' 	=> esc_html__( 'Title of the addons ', 'bigshow' ),	
				"dependency" => array(
			        "element" => "style",
			        "value" => "2"
			    ),
			),
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("page Link","bigshow"),
				"param_name"	=> "link",	
				'description' 	=> esc_html__( 'Post page link', 'bigshow' ),
				"dependency" => array(
			        "element" => "style",
			        "value" => "2"
			    ),
			),
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("Post Number","bigshow"),
				"param_name"	=> "post_num",	
				'description' 	=> esc_html__( 'Post Number for view Movie Post', 'bigshow' ),	
			),
		)
	));
}