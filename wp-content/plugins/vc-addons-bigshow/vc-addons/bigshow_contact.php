<?php 
add_shortcode( 'bigshow_form', function($atts, $content = null) {

	extract(shortcode_atts(
		array(
			"shortcode"  => '',
			"title"		 =>'',			
		),$atts));
	$output = "";
	$output ='<h2 class="contact-title">'.esc_html($title).'</h2>';
	$output .= do_shortcode(html_entity_decode(vc_value_from_safe( $shortcode, true )));	
	return $output;
	
});


if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Contact","bigshow"),
		"base"			=> "bigshow_form",
		"class"			=> "",
		"description"	=>esc_html__("Contact Form","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("Title","bigshow"),
				"param_name"	=> "title",	
				'description' 	=> esc_html__( 'Contact Title', 'bigshow' ),	
			),			
			array(
				"type"		=>"textarea_safe",
				"heading"	=> esc_html__("Shortcord","bigshow"),
				"param_name"=> "shortcode",	
				'description' => esc_html__( 'Form Shortcode', 'bigshow' ),	
			),			
		)
	));
}


?>