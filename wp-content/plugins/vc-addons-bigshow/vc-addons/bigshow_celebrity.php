<?php
add_shortcode( 'bigshow_celebrity', function($atts, $content = null) {
	
	extract(shortcode_atts(array(		
		'post_num'				=>'',
		'id'				=>'',
		'class'				=>'',
		),$atts));
		global $post;
		
		$output = '';
		$post_num = (!empty($post_num)) ? $post_num: 3;
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$query = new WP_Query(array('posts_per_page' => -1,'post_type' => 'celebrity','paged' => $paged)); 
		$count_posts = wp_count_posts('celebrity');

		$published_posts = $count_posts->publish;
		$output.='<div class="row">';
            $output.='<div class="celebrity-list">';
				while($query->have_posts()) : $query->the_post();
					$crew_type = get_post_meta( get_the_ID(), 'crew_type', 1 );
					$output.='<div class="col-md-3 col-sm-4 col-xs-6">';
						$output.='<div class="celebrity-item">';
							if ( has_post_thumbnail()) { 
                                $output.='<div class="thumb-wrap">';
                                    $output.= get_the_post_thumbnail(get_the_ID(),'full', array('class' => 'img-responsive'));
                                    $output.='<div class="thumb-hover">';
                                        $output.='<a class="celebrity-link" href="'.esc_url(get_the_permalink()).'"></a>';
                                    $output.='</div>';
                                $output.='</div>';
                            }
                           $output.=' <div class="celebrity-details">';
                                $output.='<h4 class="celebrity-name"><a href="'.esc_url(get_the_permalink()).'">'.get_the_title().'</a></h4>';
                                $output.='<p class="celebrity-profession">'.esc_html($crew_type).'</p>';
                            $output.='</div>';
                        $output.='</div>';
					$output.='</div>';
				endwhile;
			$output.='</div>';
		$output.='</div>';
		$output.='<nav class="navigation pagination" role="navigation">';
            $output.='<div class="nav-links">';
				$output.= bigshow_pagination('<i class="fa fa-caret-left"></i>','<i class="fa fa-caret-right"></i>',floor($published_posts/$post_num));
			$output.='</div>';
		$output.='</nav>';
		return $output;
		wp_reset_query(); 
});

if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Celebrity","bigshow"),
		"base"			=> "bigshow_celebrity",
		"class"			=> "",
		"description"	=>esc_html__("Type Celebrity Post Number To Show","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(
			array(
					"type"			=>"textfield",
					"heading"		=> esc_html__("Post Number","bigshow"),
					"param_name"	=> "post_num",	
					'description' 	=> esc_html__( 'Post Number for view Celebrity Information', 'bigshow' ),	
				),
			)
		)
	);
}