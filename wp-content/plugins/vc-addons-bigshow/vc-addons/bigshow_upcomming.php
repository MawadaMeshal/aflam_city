<?php
add_shortcode( 'bigshow_trailer', function($atts, $content = null) {
	
	extract(shortcode_atts(array(		
		'data'			=>'',
		'title'			=>'',
		
		),$atts));
		global $post;
		$top=$down=''; 
		$datas = (vc_param_group_parse_atts($atts['data']));
		
		$output = '';
		$output.='<div class="section section-padding bg-image upcomming-section text-white">';
            $output.='<div class="container">';
                $output.='<div class="row">';
                    $output.='<div class="col-md-8">';
                        $output.='<div class="section-header">';
                            $output.='<h2 class="section-title">'.esc_html($title).'</h2>';
                        $output.='</div>';
                   $output.=' </div>';
                $output.='</div>';

                $output.='<div class="row">';
                	$p=1;
                	if(!empty($datas) && is_array($datas)){
                		foreach($datas as $data){
                			if($p==1){
                				$top.='<div class="col-md-9">';
			                        $top.='<div class="upcomming-featured">';
			                            $top.='<img class="img-responsive" src="'.esc_url(wp_get_attachment_url($data["data_image"])).'" alt="'.esc_html__("Upcoming Featured","bigshow").'">';
			                            $top.='<a class="play-video" href="'.esc_url($data['data_trailer']).'"><i class="fa fa-play"></i></a>';
			                            $top.='<div class="upcomming-details">';
			                                $top.='<h4 class="video-title"><a href="#">'.esc_html($data['data_title']).'</a></h4>';
			                                $top.='<p class="video-release-on">'.esc_html($data['data_date']).'</p>';
			                            $top.='</div>';
			                        $top.='</div>';
			                    $top.='</div>';

                			}else{                				
								$down.='<div class="upcomming-item">';
									$down.='<img class="img-responsive" src="'.esc_url(wp_get_attachment_url($data["data_image"])).'" alt="'.esc_html__("Upcoming Featured","bigshow").'">';
									$down.='<div class="upcomming-details">';
										$down.='<h4 class="video-title"><a href="'.get_the_permalink().'">'.esc_html($data['data_title']).'</a></h4>';
										$down.='<p class="video-release-on">'.esc_html($data['data_date']).'</p>';
									$down.='</div>';
									$down.='<div class="upcomming-hover">';
										$down.='<a class="play-video" href="'.esc_url($data['data_trailer']).'"><i class="fa fa-play"></i></a>';
									$down.='</div>';
								$down.='</div>';			                    
                			}
                			$p++;
                		}
                	}
					$output.= $top;
					$output.= '<div class="col-md-3 col-xs-12 sm-top-30">'.$down.'</div>';                                   
               $output.=' </div>';
            $output.='</div>';
        $output.='</div>';
		return $output;

});


if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Upcoming Movie ","bigshow"),
		"base"			=> "bigshow_trailer",
		"class"			=> "",
		"description"	=>esc_html__("Display Upcoming Movie ","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(
				array(
					"type"			=>"textfield",
					"heading"		=> esc_html__("title","bigshow"),
					"param_name"	=> "title",	
					'description' 	=> esc_html__( 'Post Title', 'bigshow' ),
					
				),
			array(
				'type' => 'param_group',
				'value' => '',
				"heading" => esc_html__("Upcoming Movie Information", 'bigshow'),
				'param_name' => 'data',
				"description"	=> esc_html__("Upcoming Movie  ","bigshow"),
				'params' => array(					
					array(
						"type"		=>"attach_image",
						"heading"	=> esc_html__("Second Image","bigshow"),
						"param_name"=> "data_image",	
						'description' => esc_html__( 'Image Size Must Be 1000 X 666px.', 'bigshow' ),
					),	
					array(
						"type"			=>"textfield",
						"heading"		=> esc_html__("title","bigshow"),
						"param_name"	=> "data_title",	
						'description' 	=> esc_html__( 'Post Title', 'bigshow' ),
						
					),array(
						"type"			=>"textfield",
						"heading"		=> esc_html__("date","bigshow"),
						"param_name"	=> "data_date",	
						'description' 	=> esc_html__( 'Post date', 'bigshow' ),
						
					),array(
						"type"			=>"textfield",
						"heading"		=> esc_html__("Trailer Url","bigshow"),
						"param_name"	=> "data_trailer",	
						'description' 	=> esc_html__( 'Post Trailer', 'bigshow' ),
						
					),
											
				)
			),		
		)
	));
}