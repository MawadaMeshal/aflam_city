<?php
add_shortcode( 'bigshow_map', function($atts, $content = null) {
	
	extract(shortcode_atts(array(
		'latitude'			=>'',
		'longitude'			=>'',		
		'id'				=>'',
		'class'				=>'',
		),$atts));
		
		$output = '';
		 
	  	$output.='<div id="map-area" class="section">
                <div class="google-map" id="location" data-lat="'.esc_html($latitude).'" data-lng="'.esc_html($longitude).'" data-zoom="16"></div>
            </div>';
        
		return $output;

});

if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Map","bigshow"),
		"base"			=> "bigshow_map",
		"class"			=> "",
		"description"	=>esc_html__("Type Map Location","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("latitude","bigshow"),
				"param_name"	=> "latitude",	
				
			),
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("longitude","bigshow"),
				"param_name"	=> "longitude",	
				
			),
			
			
		)
	));
}