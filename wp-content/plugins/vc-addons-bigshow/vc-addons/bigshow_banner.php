<?php
add_shortcode( 'bigshow_banner', function($atts, $content = null) {
	
	extract(shortcode_atts(array(		
		
		'banner'			=>'',
		),$atts));
		$output = '';		
		$output.='<div class="banner-slider-area text-white owl-theme">';
	        $output.='<div id="banner-slider" class="owl-carousel banner-slider">';
	             $banner = explode(',', $banner);
	             
	            $query = new WP_Query( array( 'post_type' => 'movie', 'post__in' =>  $banner ) );
			   
			    if($query->have_posts()){
			    	$i = 1;
	                while($query->have_posts()) { $query->the_post();
	                	$id = rand().$i;
	                	$url = get_the_post_thumbnail_url(get_the_ID(),'full');
	                	
						$movie_arr =array("movie_trailer",
						        "movie_rating",
						        "movie_vote",
						);

						foreach ($movie_arr as $key => $value) {
						   $data = get_post_meta( get_the_ID(), $value, 1 );
						   $movie_arr[$value]  = !empty( $data) ?  $data : ''; 
						}

						extract($movie_arr);
						$date = get_the_date('d F , Y');
			           
			            $output.='<div style="background-image: url('.esc_url($url).')" class="banner-item banner-item-'.esc_html($id).'">';
			                $output.='<div class="overlay-70">';
			                    $output.='<a class="play-video" href="'.esc_url(get_the_permalink()).'"><i class="fa fa-play"></i></a>';
			                    $output.='<div class="banner-content">';
			                        $output.='<div class="container">';
			                            $output.='<div class="row">';
			                                $output.='<div class="col-lg-4 col-md-6">';
			                                    $output.='<span class="rating">'.esc_html($movie_rating).'</span>';
			                                    $output.='<h1 class="banner-title"><a href="'.esc_url(get_the_permalink()).'">'.get_the_title().'</a></h1>';
			                                    $output.='<p class="banner-date-meta">'.esc_html($date).'</p>';
			                                $output.='</div>';
			                           $output.=' </div>';
			                       $output.=' </div>';
			                    $output.='</div>';
			                $output.='</div>';
			            $output.='</div>';
		      		$i++;
		      		}
		      		wp_reset_query() ;
		        }
	        $output.='</div>';
	    $output.='</div>';
		return $output;

});


if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Banner","bigshow"),
		"base"			=> "bigshow_banner",
		"class"			=> "",
		"description"	=>esc_html__("Please Give for show banner","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(

			array(
				"type"			=>"bigshow_custom_field",
				"heading"		=> esc_html__("Banner","bighsow"),
				"param_name"	=> "banner",
				"value"			=> post_name_show(),
				
			),
		)
	));
}