<?php
add_shortcode( 'bigshow_address', function($atts, $content = null) {

	extract(shortcode_atts(array(
	
		'title'			=>'',
		'description'	=>'',
		'info'			=>'',
		
		
		),$atts));
		$output = '';
		$datas = (vc_param_group_parse_atts($atts['info']));

		$output .='<h2 class="contact-detail-title">'.esc_html($title).'</h2>';
		$output .='<p>'.esc_html($description).'</p>';
		$output .=' <div class="contact-infos">';
		if(!empty($datas) && is_array($datas)){
			foreach($datas as $data){
	           $output .=' <div class="contact-info-item">';
	                $output .='<label><b>'.esc_html($data["address"]).'</b></label>';
	                $output .='<p>'.esc_html($data["content"]).'</p>';
	            $output .='</div>';
	        }
	    }

        $output .='</div>';
    return $output;
});
if(class_exists('WPBakeryVisualComposerAbstract') ) {

	vc_map(array(
		"name"			=> esc_html__("Bigshow Address","Bigshow"),
		"base"			=> "bigshow_address",
		"description"	=>esc_html__("Bigshow Address Information ","Bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","Bigshow"),
		"params"		=>array(
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("Title","Bigshow"),
				"param_name"	=> "title",	
				'description' 	=> esc_html__( 'Title', 'Bigshow' ),	
			),
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("Description","Bigshow"),
				"param_name"	=> "description",	
				'description' 	=> esc_html__( 'Address Title', 'Bigshow' ),	
			),
			array(
				'type' 			=> 'param_group',
				'value' 		=> '',
				"heading" 		=> esc_html__("Address information", 'Bigshow'),
				'param_name' 	=> 'info',								
				'params' 		=> array(
					array(
						"type"			=>"textfield",
						"heading"		=> esc_html__("Address Title","Bigshow"),
						"param_name"	=> "address",	
						'description' 	=> esc_html__( 'Address Title', 'Bigshow' ),	
					),	
					array(
						"type"			=>"textfield",
						"heading"		=> esc_html__("Address Content","Bigshow"),
						"param_name"	=> "content",	
						'description' 	=> esc_html__( 'Address Content', 'Bigshow' ),	
					),							
				)
			),
				
		)
	)
);}