<?php
add_shortcode( 'bigshow_post', function($atts, $content = null) {
	
	extract(shortcode_atts(array(		
		'post_no'		=>'',
		'title'			=>'',
		'post_link'		=>'',
		
		),$atts));
		global $post;
		$post_no = !empty($post_no) ? $post_no : 4;
		$args = array( 'posts_per_page' => $post_no,'post_type' => 'post' );
		
		$recent_posts_list = new WP_Query( $args );
		$output = '';		
		$output.='<div class="section section-padding top-padding-normal news-section">';
            $output.='<div class="container">';
                $output.='<div class="row">';
                    $output.='<div class="col-xs-8">';
                        $output.='<div class="section-header">';
                            $output.='<h2 class="section-title">'.esc_html($title).'</h2>';
                        $output.='</div>';
                    $output.='</div>';
                    $output.='<div class="col-xs-4">';
                        $output.='<a class="all-link" href="'.esc_url($post_link).'">'.esc_html__("See All News","bigshow").'</a>';
                    $output.='</div>';
                $output.='</div>';
                $output.='<div class="row">';
                	if($recent_posts_list->have_posts()){
                		$p = 1;
						while($recent_posts_list->have_posts()) : $recent_posts_list->the_post();
		                    $date = get_the_date('d F , Y');
		                    if($p == 1){
			                    $output.='<div class="col-md-6">';
			                        $output.='<a class="news-featured" href="'.get_the_permalink().'">';
										$post_poster = get_post_meta( get_the_ID(), 'post_poster', 1 );
										$post_poster =  !empty($post_poster) ? $post_poster :get_the_post_thumb('full');
										
										$output.='<img src="'. esc_url($post_poster).'" alt="'.esc_html__("Movie Thumb","bigshow").'" class="img-responsive">';
										
										$output.='<span class="news-date-meta">'.esc_html($date).'</span>';
										$output.='<div class="news-content">';
											$output.='<h4 class="news-title">'.get_the_title().'</h4>';
										$output.='</div>';
			                        $output.='</a>';
			                    $output.='</div>';
			                } else {
			                    $output.='<div class="col-md-6 sm-top-30">';
									
			                        $output.='<div class="news-item">';
										$post_poster = get_post_meta( get_the_ID(), 'post_poster', 1 );
			                           if ( !empty($post_poster)) {										    
				                            $output.='<a class="news-thumb" href="'.get_the_permalink().'">';
												$output.='<img src="'.esc_url($post_poster).'" class="img-responsive" alt="Movie Thumb">';				                                
				                            $output.='</a>';
				                        }
			                            $output.='<div class="news-content">';
			                                $output.='<span class="news-date-meta">'.esc_html($date).'</span>';
			                                $output.='<h4 class="news-title"><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
											$output.='<p class="news-excerpt">'.bigshow_excerpt(24).'</p>';
			                            $output.='</div>';
			                        $output.='</div>';
			                    $output.='</div>';			                    
			                }
			                $p++;
		                endwhile;
		                wp_reset_query() ;
		            }
                $output.='</div>';
            $output.='</div>';
        $output.='</div>';
		return $output;

});


if(class_exists('WPBakeryVisualComposerAbstract') ) {
	vc_map(array(
		"name"			=> esc_html__("Bigshow Blog Post ","bigshow"),
		"base"			=> "bigshow_post",
		"class"			=> "",
		"description"	=>esc_html__("Display Upcoming Movie ","bigshow"),
		"category"		=> esc_html__("Bigshow Shortcord ","bigshow"),
		"params"		=>array(
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("title","bigshow"),
				"param_name"	=> "title",	
				'description' 	=> esc_html__( 'Post Title', 'bigshow' ),
				
			),
			array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("post no","bigshow"),
				"param_name"	=> "post_no",	
				'description' 	=> esc_html__( 'Post Number for display', 'bigshow' ),
				
			),array(
				"type"			=>"textfield",
				"heading"		=> esc_html__("post page link","bigshow"),
				"param_name"	=> "post_link",	
				'description' 	=> esc_html__( 'All Post page', 'bigshow' ),
				
			),

			
		)
	));
}