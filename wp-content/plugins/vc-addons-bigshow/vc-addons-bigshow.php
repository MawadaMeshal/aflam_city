<?php
/**
 * @category     WordPress_Plugin
 * @package      Bighsow AddOns
 * @author       Codepassenger
 * Plugin Name:  Bighsow Visual Composser AddOns
 * Author:       Codepassenger
 * Author URI:   http://Codepassenger.com
 * Text Domain:  Bighsow
 * **********************************************************************
 */
 
define( 'BIGSHOW_PLUGIN_VISUAL_COMPOSER_ACTIVED', in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'BIGSHOW_CONTACT_ACTIVE', in_array( 'contact-form-7/wp-contact-form-7.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
/*
	this action add for set page priority
*/
add_action('init','bigshow_file',20);

function bigshow_file(){
	if(BIGSHOW_PLUGIN_CUSTOM_POST){
		require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow_celebrity.php";
	 	require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow_movie.php";
	 	
	}
	require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow_banner.php";
	require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow_upcomming.php";
	require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow_post.php";
	
	require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow-map.php";
	require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow-address.php";
	if(BIGSHOW_CONTACT_ACTIVE){
		require_once plugin_dir_path( __FILE__ )."vc-addons/bigshow_contact.php";
		
	}
	function bigshow_loadscript(){
		$key = get_theme_mod('apikey_data');				
		$key = !empty($key) ? "?key=".$key : '';				
		wp_enqueue_script('bigshow-google-map','https://maps.googleapis.com/maps/api/js'.$key , array('jquery'),false,true);
	}
	add_action('wp_enqueue_scripts','bigshow_loadscript');
}
	function tag_show(){
		$data = array();
		$taxonomy = 'movie_categories';
		$terms = get_terms($taxonomy);
		if ( $terms && !is_wp_error( $terms ) ) :
			foreach ($terms as $term) {
				$data[$term->slug] = $term->name;
			}
		endif;
		return $data;
	}
	function post_name_show(){
		$data = array();
		$args = array('post_type' => 'movie',);
		$loop = new WP_Query($args);

		while($loop->have_posts()): $loop->the_post();

			$title = get_the_title();
			$id = get_the_ID();
			$data[$title]=$id;

		endwhile;
		return $data;
		
	}
	vc_add_shortcode_param( 'bigshow_custom_field', 'bigshow_param_settings_fields' );
	function bigshow_param_settings_fields( $settings, $value ) {
		$markup = '';

		$arr = explode(',',$value);
		$markup .= '<div class="my_param_block">';
			$markup .= '<select " class="wpb_vc_param_value wpb-input wpb-select align dropdown align_center ' .
				 esc_attr( $settings['param_name'] ) . ' ' .
				 esc_attr( $settings['type'] ) . '_field"  name="'. esc_attr( $settings['param_name'] ) .'" multiple>';
				$opt = $settings['value'];
				if(is_array($opt)){
					if(!empty($opt)){
						foreach($opt as $key=>$vl){
							$selected = (in_array($vl,$arr))?'selected="selected"':'';
							$markup .= '<option '.$selected.'  value="'.$vl.'">'.$key.'</option>';
						}
					}
				}
			$markup .= '</select>';
		$markup .= '</div>'; // This is html markup that will be outputted in content elements edit form
		return $markup;
	}
		
 