<?php 
if ( !defined('ABSPATH') )
    exit;

/*
  Plugin Name: Bigshow Post Share
  Plugin URI: http://codepassenger.com/
  Description: Share your blog post in Social Network
  Version: 1.0
  Author: codepassenger
  Author URI: http://codepassenger.com
  Text Domain:  bigshow

 * @category WordPress_Plugin
 * @since Bigshow 1.0
 * @package Bigshow
*/

if(!function_exists('bigshow_post_share')){
	function bigshow_post_share(){
		global $post;
		// Get current page URL 
		$crunchifyURL = get_permalink();
		$output = '';
		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL.'&amp;via=Crunchify';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$linkedinURL = 'http://www.linkedin.com/shareArticle?mini=true&amp;title'.$crunchifyTitle.'=&amp;url='.$crunchifyURL;
		$output .= '<div class="share-social">';
		$output .= '<a href="'.esc_url($facebookURL).'" target="_blank"><i class="fa fa-facebook"></i></a>
					<a class="share-on-facebook" href="'.esc_url($twitterURL).'" target="_blank"><i class="fa fa-twitter"></i></a>
					<a class="share-on-twitter" href="'.esc_url($linkedinURL).'" target="_blank"><i class="fa fa-linkedin"></i></a>
					<a class="share-on-google-plus" href="'.esc_url($googleURL).'" target="_blank"><i class="fa fa-google-plus"></i></a>';
		$output .= '</div>';
		return $output;
	}
}

add_shortcode('cp_post_share','bigshow_post_share');


