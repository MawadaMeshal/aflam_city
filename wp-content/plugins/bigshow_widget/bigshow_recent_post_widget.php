<?php
	add_action('widgets_init', 'bigshow_recent_post');

	function bigshow_recent_post() {
		register_widget('bigshow_recent_post');
	}
	class bigshow_recent_post extends WP_Widget{
		public function __construct(){
			parent::__construct('bigshow_recent_post',__('bigshow Recent Posts','bigshow'),array(
			'description' => __('Add Recent Post Widget','bigshow'),
			));
		}
		public function widget($args,$instance){
			
			
				$title = $args['before_title'];
				$title .= $instance['title'];
				$title .=$args['after_title'];
				print $args['before_widget'];
				$date = get_the_date('d F , Y');
				$wi_title 	= !empty($instance['title'])?$instance['title']:'Title';
				
			?>			
			<h3 class="widget-title"><?php print esc_html($wi_title);?></h3>
			<div class="widget-inner">
                <div class="widget-posts">
                	<?php 
						$data = array('post_type'=>'post','numberposts'=>$instance['posts']);
						$recentPost = wp_get_recent_posts( $data );
						foreach($recentPost as $recentPosts){
					?>
	                    <div class="widget-post">
	                    	<a class="widget-thumb" href="<?php print esc_url($recentPosts['guid']); ?>">
							<?php
								if(has_post_thumbnail($recentPosts['ID'])){
									$image = wp_get_attachment_image_src( get_post_thumbnail_id($recentPosts['ID']),'bigshow_recent_thumb');
								?>
									<img src="<?php echo esc_url($image[0]);?>" class="img-responsive" alt="<?php esc_attr_e('blog','bighsow'); ?>" >
									<?php
								}
							?>
							</a>
							<div class="widget-post-content">
								<a class="widget-post-title" href="<?php print esc_url($recentPosts['guid']); ?>"><?php print esc_html($recentPosts['post_title']); ?></a>
								<p class="widget-post-date"><?php print esc_html($date);?></p>
							</div>
	                    </div>
                    <?php } ?>
                </div>
            </div>
			<?php
			print $args['after_widget'];			
		}
		
		public function form($instance){
			$title  = isset($instance['title']) ? $instance['title']:' ';
			$posts = !empty($instance['posts']) ? $instance['posts']:5;
			
		?>
			<p>
				<label for="title"><?php echo esc_html("Title:")?></label>
			</p>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>"  name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>">
			<p>
				<label for="posts"><?php echo esc_html("Number of posts to show:")?></label>
			</p>
			<input type="text" id="<?php echo $this->get_field_id('posts'); ?>"  name="<?php echo $this->get_field_name('posts'); ?>" value="<?php echo $posts; ?>">
			
		<?php
		}
	}