<?php
/**
 * @category     WordPress_Plugin
 * @package      bigshow Widget
 * @author       Codepassenger
 * Plugin Name:  bigshow Custom Widget
 * Author:       Codepassenger
 * Author URI:   http://Codepassenger.com
 * Text Domain:  bigshow
 * **********************************************************************
 */

define( 'BIGSHOW_MAILCHAPY_ACTIVED', in_array( 'mailchimp-for-wp/mailchimp-for-wp.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
 require_once plugin_dir_path( __FILE__ )."bigshow_aboutus_widget.php"; 
 require_once plugin_dir_path( __FILE__ )."bigshow_recent_post_widget.php"; 
 require_once plugin_dir_path( __FILE__ )."category-widget.php";
 if(BIGSHOW_MAILCHAPY_ACTIVED){
	require_once plugin_dir_path( __FILE__ )."bigshow_appstore.php";
 }
function tag_show_list(){
	$data = array();
	$taxonomy = 'movie_categories';
	$terms = get_terms($taxonomy,array('hide_empty'=>false));

	if ( $terms && !is_wp_error( $terms ) ) :
		return $terms;
	endif;
	return array();
}

	
