<?php
	
	/**
	 * bigshow Appstore Widget
	 *
	 * Displays Appstore widget
	 *
	 * @author 		Codepassenger
	 * @category 	Widgets
	 * @package 	bigshow/Widgets
	 * @version 	1.0.0
	 * @extends 	WP_Widget
	 */

	add_action('widgets_init', 'bigshow_appstore_widget');

	function bigshow_appstore_widget() {
		register_widget('bigshow_appstore_widget');
	}
	class bigshow_appstore_widget  extends WP_Widget{
		public function __construct(){
			parent::__construct('bigshow_appstore_widget',esc_html__('Bigshow Appstore Widget','bigshow'),array(
			'description' => esc_html__('Bigshow Appstore Widget','bigshow'),
			));
		}
		public function form($instance){
			$title  	= isset($instance['title'])? $instance['title']:' ';
			$s_desc  = isset($instance['s_desc'])? $instance['s_desc']:'';
			$mialchamp  		= isset($instance['mialchamp'])? $instance['mialchamp']:' ';
			$apptitle  		= isset($instance['apptitle'])? $instance['apptitle']:' ';

			$playstore  	= isset($instance['playstore'])? $instance['playstore']:' ';
			$iosstore  	= isset($instance['iosstore'])? $instance['iosstore']:' ';
			
			?>
			<p>
				<label for="title"><?php esc_html_e('Title:','bigshow'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>"  name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($title); ?>">
			<p>
				<label for="title"><?php esc_html_e('Short Description:','bigshow'); ?></label>
			</p>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo esc_attr($this->get_field_id('s_desc')); ?>" value="<?php echo esc_attr($s_desc); ?>" name="<?php echo esc_attr($this->get_field_name('s_desc')); ?>"><?php echo esc_attr($s_desc); ?></textarea>
			<p>
				<label for="title"><?php esc_html_e('Mailchamp Shortcode:','bigshow'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('mialchamp')); ?>"  name="<?php echo esc_attr($this->get_field_name('mialchamp')); ?>" value="<?php echo esc_attr($mialchamp); ?>">
			<p>
				<label for="title"><?php esc_html_e('App Title:','bigshow'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('apptitle')); ?>"  name="<?php echo esc_attr($this->get_field_name('apptitle')); ?>" value="<?php echo esc_attr($apptitle); ?>">
			
			<p>
				<label for="title"><?php esc_html_e('playstore:','bigshow'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('playstore')); ?>"  name="<?php echo esc_attr($this->get_field_name('playstore')); ?>" value="<?php echo esc_attr($playstore); ?>">
			<p>
				<label for="title"><?php esc_html_e('iosstore playstore:','bigshow'); ?></label>
			</p>
			<input type="text" id="<?php echo esc_attr($this->get_field_id('iosstore')); ?>"  name="<?php echo esc_attr($this->get_field_name('iosstore')); ?>" value="<?php echo esc_attr($iosstore); ?>">
			
			<?php
		}
		public function widget($args,$instance){
			
			$wi_title 	= !empty($instance['title'])?$instance['title']:'Title';
			$s_desc  = isset($instance['s_desc'])? $instance['s_desc']:'';
			$playstore 	= !empty($instance['playstore'])?$instance['playstore']:'';
			$iosstore 	= !empty($instance['iosstore'])?$instance['iosstore']: '';
			$mialchamp 	= !empty($instance['mialchamp'])?$instance['mialchamp']: '';
			$apptitle 	= !empty($instance['apptitle'])?$instance['apptitle']: '';
			?>
			<?php 
				print $args['before_widget']; 
			?>
				
				<h3 class="widget-title"><?php print esc_html($wi_title);?></h3>
				<div class="widget-inner">
					<p><?php print esc_html($s_desc); ?></p>
					<?php echo do_shortcode($mialchamp); ?>
				</div>
	            <?php  print $args['after_widget']; ?>
	            <?php print $args['before_widget']; ?>
		            <h3 class="widget-title"><?php echo esc_html($apptitle); ?></h3>
	                <div class="widget-inner">
	                    <a class="google-play-download" href="<?php echo esc_url($playstore);?>"><img src="<?php echo get_template_directory_uri().'/images/google-play.png'; ?>" alt="<?php esc_attr_e('image','bigshow'); ?>"></a>
	                    <a class="apple-store-download" href="<?php echo esc_url($iosstore);?> "><img src="<?php echo get_template_directory_uri().'/images/app-store.png';?>" alt="<?php esc_attr_e('image','bigshow'); ?>"></a>
	                </div>
           		<?php  print $args['after_widget']; 
		}


		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			$instance['apptitle'] = ( ! empty( $new_instance['apptitle'] ) ) ? strip_tags( $new_instance['apptitle'] ) : '';
			$instance['playstore'] = ( ! empty( $new_instance['playstore'] ) ) ? strip_tags( $new_instance['playstore'] ) : '';
			$instance['iosstore'] = ( ! empty( $new_instance['iosstore'] ) ) ? strip_tags( $new_instance['iosstore'] ) : '';
			$instance['mialchamp'] = ( ! empty( $new_instance['mialchamp'] ) ) ? strip_tags( $new_instance['mialchamp'] ) : '';
			$instance['s_desc'] = ( ! empty( $new_instance['s_desc'] ) ) ? strip_tags( $new_instance['s_desc'] ) : '';
			
			return $instance;
		}

	}