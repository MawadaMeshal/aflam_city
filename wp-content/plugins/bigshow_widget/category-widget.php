<?php
	add_action('widgets_init', 'bigshow_video_cat');

	function bigshow_video_cat() {
		register_widget('bigshow_video_cat');
	}
	class bigshow_video_cat extends WP_Widget{
		public function __construct(){
			parent::__construct('bigshow_video_cat',__('bigshow Video Category','bigshow'),array(
			'description' => __('Add Video Category Widget','bigshow'),
			));
		}
		public function widget($args,$instance){
			
			
				$title = $args['before_title'];
				$title .= $instance['title'];
				$title .=$args['after_title'];
				print $args['before_widget'];
				$date = get_the_date('d F , Y');
				$wi_title 	= !empty($instance['title'])?$instance['title']:'Title';
				$posts_no = !empty($instance['posts']) ? $instance['posts'] : 10;
				$tag = tag_show_list();
				
				$data = array_slice($tag, 0,$posts_no);
				
			?>			
			<h3 class="widget-title"><?php print esc_html($wi_title);?></h3>
			<div class="widget-inner">
                <ul class="widget-cat">
                	<?php 	foreach ($data as $value) { ?>
						<li class="cat"><a href="<?php echo esc_url(get_term_link($value)); ?>"><?php echo esc_html($value->name); ?></a></li>
					<?php	}	?>
                </ul>
            </div>
			<?php
			print $args['after_widget'];			
		}
		
		public function form($instance){
			$title  = isset($instance['title']) ? $instance['title']:' ';
			$posts = !empty($instance['posts']) ? $instance['posts']:10;
			
		?>
			<p>
				<label for="title"><?php echo esc_html("Title:")?></label>
			</p>
			<input type="text" id="<?php echo $this->get_field_id('title'); ?>"  name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>">
			<p>
				<label for="posts"><?php echo esc_html("Number for show Category :")?></label>
			</p>
			<input type="text" id="<?php echo $this->get_field_id('posts'); ?>"  name="<?php echo $this->get_field_name('posts'); ?>" value="<?php echo $posts; ?>">
			
		<?php
		}
	}